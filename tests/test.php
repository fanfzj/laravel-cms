create table cms_advcontent(
	id int key auto_increment,
	cid int,
	advposid int,
	path varchar(255),
	content text,
	createtime datetime,
	updatetime datetime
);
create table cms_site(
	id int key auto_increment,
	sitename varchar(100),
	indextemplate varchar(50),
	beian varchar(100),
	powerby text,
	keyword text,
	des text
);
create table cms_friend(
	id int key auto_increment,
	fname varchar(20),
	flogo varchar(255),
	cid int
);