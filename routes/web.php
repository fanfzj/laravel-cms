<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', "IndexController@index");
Route::get('admin/login', "Admin\UserController@login");
Route::get('admin/logout', "Admin\UserController@logout");
Route::post('admin/check', "Admin\UserController@check");
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'login'], function () {
	Route::get('/', "IndexController@index");
	//用户
	Route::get('user/index', "UserController@index");
	Route::get('user/add', "UserController@add");
	Route::post('user/doadd', "UserController@doadd");
	Route::get('user/edit/{id}', "UserController@edit");
	Route::post('user/doedit', "UserController@doedit");
	Route::get('user/del/{id}', "UserController@del");
	Route::get('user/password', "UserController@edit_passwd");
	Route::post('user/dopassword', "UserController@doedit_passwd");
	//权限
	Route::get('roles/index', "RolesController@index");
	Route::get('roles/add', "RolesController@add");
	Route::post('roles/doadd', "RolesController@doadd");
	Route::get('roles/edit/{id}', "RolesController@edit");
	Route::post('roles/doedit', "RolesController@doedit");
	Route::get('roles/del/{id}', "RolesController@del");
	//设置
	Route::get('seting', "SetingController@edit");
	Route::post('seting/doedit', "SetingController@doedit");
	//栏目
	Route::get('category/add', "CategoryController@add");
	Route::post('category/doadd', "CategoryController@doadd");
	Route::get('category/index', "CategoryController@index");
	Route::get('category/edit/{id}', "CategoryController@edit");
	Route::post('category/doedit', "CategoryController@doedit");
	Route::get('category/del/{id}', "CategoryController@del");
	//内容
	Route::get('content/index/{cid?}', "ContentController@index");
	Route::get('content/add/{cid?}', "ContentController@add");
	Route::post('content/doadd', "ContentController@doadd");
	Route::get('content/edit/{id}/{cid}', "ContentController@edit");
	Route::get('content/delimage', "ContentController@delimage");
	Route::post('content/doedit', "ContentController@doedit");
	Route::get('content/del/{id}/{cid}', "ContentController@del");
	//数据库
	Route::get('data/recovery', "DataController@recovery");
	Route::post('data/dorecovery', "DataController@dorecovery");

	Route::get('data/backup', "DataController@backup");
	Route::post('data/dobackup', "DataController@dobackup");
	//资源
	Route::get('resources/index', "ResoucesController@index");
	Route::post('resources/upload', "ResoucesController@upload");
	Route::get('resources/del', "ResoucesController@del");
	//友链
	Route::get('friend/index', "FriendController@index");
	Route::get('friend/add', "FriendController@add");
	Route::post('friend/doadd', "FriendController@doadd");
	Route::get('friend/edit/{id}', "FriendController@edit");
	Route::post('friend/doedit', "FriendController@doedit");
	Route::get('friend/del/{id}', "FriendController@del");
	//广告位置
	Route::get('advpos/index', "AdvposController@index");
	Route::get('advpos/add', "AdvposController@add");
	Route::post('advpos/doadd', "AdvposController@doadd");
	Route::get('advpos/edit/{id}', "AdvposController@edit");
	Route::post('advpos/doedit', "AdvposController@doedit");
	Route::get('advpos/del/{id}', "AdvposController@del");
	//广告内容
	Route::get('adv/index', "AdvController@index");
	Route::get('adv/add', "AdvController@add");
	Route::post('adv/doadd', "AdvController@doadd");
	Route::post('adv/upload', "AdvController@upload");
	Route::get('adv/getadvpos', "AdvController@getadvpos");
	Route::get('adv/delimage', "AdvController@delimage");
	Route::get('adv/del/{id}', "AdvController@del");
	Route::get('adv/edit/{id}', "AdvController@edit");
	Route::post('adv/doedit', "AdvController@doedit");
	//模型
	Route::get('models/add', "ModelsController@add");
	Route::post('models/doadd', "ModelsController@doadd");
	Route::get('models/index', "ModelsController@index");
	Route::get('models/edit/{id}', "ModelsController@edit");
	Route::post('models/doedit', "ModelsController@doedit");
	Route::get('models/detaildel/{id}', "ModelsController@detaildel");
	Route::get('models/del/{id}', "ModelsController@del");
});
Route::get("{tablename}/{pagestyle}/{cid}", "HomeController@all");
Route::get("search/{keyword}", "HomeController@search");
Route::get("{enname}/{id}", "HomeController@detail");
