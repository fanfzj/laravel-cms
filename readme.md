# Larval_Cms

#### 介绍
CMS（Content Management System）是一个网站内容管理系统，该系统主要用来解决各种非结构化或半结构化数字资源的采集、管理、利用、传递和增值，并将这些资源有机结成到结构化数据的商业智能环境中，如办公自动化系统（OA系统）、客户关系管理系统（CRM系统）等。内容的创作人员、编辑人员和发布人员均可使用内容管理系统（CMS）来提交、修改、审批和发布内容。这些“内容”可能包括文件、表格、图片、数据库中的数据甚至视频等一切用户用来发布到对接网站的信息。

#### 软件架构
软件架构说明


#### 运行教程

1.  mysql新建数据库cms,然后导入sql文件（storage\database下）
2.  项目根目录下，cmd中执行php artisan serve

#### nginx部署

[部署](https://learnku.com/docs/laravel/5.8/deployment/3884)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
