-- mysqldump-php https://github.com/ifsnop/mysqldump-php
--
-- Host: localhost	Database: ucms
-- ------------------------------------------------------
-- Server version 	5.7.26
-- Date: Thu, 22 Apr 2021 09:54:38 +0800

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cms_advcontent`
--

DROP TABLE IF EXISTS `cms_advcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_advcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `advposid` int(11) DEFAULT NULL,
  `path` text,
  `content` text,
  `createtime` datetime DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_advcontent`
--

LOCK TABLES `cms_advcontent` WRITE;
/*!40000 ALTER TABLE `cms_advcontent` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_advcontent` VALUES (3,4,3,'|adv/k3s2EzPhwUSdzGpBor2n1Zxo5jUTqcGLPiZqEoV8.jpeg|adv/eZ64btCdVBEJFOVKeiLPY04CnnVNeQrS9BEAtcKD.jpeg|','测试','2019-01-05 20:52:41','2019-01-05 20:52:41'),(4,0,2,'|adv/L2uAmby93dVZ3ZJkUxuWiOnwVXAHB4qA6btJKUnW.png|adv/m4lLOna3JgwwUhnkyT1prXV0zbHDohy7cyIyO2YH.jpeg|adv/lacmwvmX4EMZwNlKe8OOGhDASVv4uyKx22oo2MaN.jpeg|','<li><a href=\'/\'><img src=\"@imageSrc0@\"></a></li>\r\n<li><a href=\'/\'><img src=\"@imageSrc1@\"></a></li>\r\n<li><a href=\'/\'><img src=\"@imageSrc2@\"></a></li>','2019-01-05 21:09:20','2019-01-06 21:11:57');
/*!40000 ALTER TABLE `cms_advcontent` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_advcontent` with 2 row(s)
--

--
-- Table structure for table `cms_advposition`
--

DROP TABLE IF EXISTS `cms_advposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_advposition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `namer` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_advposition`
--

LOCK TABLES `cms_advposition` WRITE;
/*!40000 ALTER TABLE `cms_advposition` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_advposition` VALUES (2,0,'banner广告'),(3,4,'测试');
/*!40000 ALTER TABLE `cms_advposition` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_advposition` with 2 row(s)
--

--
-- Table structure for table `cms_category`
--

DROP TABLE IF EXISTS `cms_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(20) DEFAULT NULL,
  `enname` varchar(50) DEFAULT NULL,
  `modelid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `isshow` tinyint(1) DEFAULT '1',
  `seotitle` varchar(255) DEFAULT NULL,
  `seokeyword` text,
  `seodes` text,
  `pagestyle` tinyint(1) DEFAULT '1',
  `coverpath` varchar(100) DEFAULT NULL,
  `listpath` varchar(100) DEFAULT NULL,
  `detailpath` varchar(100) DEFAULT NULL,
  `content` text,
  `ordernum` int(11) DEFAULT '9999',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `cname` (`cname`) USING BTREE,
  UNIQUE KEY `enname` (`enname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_category`
--

LOCK TABLES `cms_category` WRITE;
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_category` VALUES (1,'单位产品','product',17,0,1,'单位产品','b-1','c-1',2,NULL,'product/lister','product/detail',NULL,9999,0),(2,'椰壳净水碳','ykjst',17,1,2,'单位产品-椰壳净水碳','b','c',2,NULL,'product/lister','product/detail',NULL,9999,0),(3,'果壳净水炭','gkjst',17,1,2,'单位产品-果壳净水炭','b','c',2,NULL,'product/lister','product/detail',NULL,9999,0),(4,'新闻动态','news',18,0,1,'新闻动态','b','c',2,NULL,'news/lister','news/detail',NULL,9999,0),(5,'关于我们','aboutus',18,0,1,'关于我们',NULL,NULL,1,'company/aboutus',NULL,NULL,'<p>优加内容管理系统（简称：U+CMS），U+CMS是一款开源的PHP+MySql的Cms，经过1年时间的市场分析和调研，于2015年7月1日正式上线。我们的产品专为几十万中小企业开展网络营销提供了源动力。</p><p>现在市场上各种各样的CMS系统遍地开花，那U+CMS跟其他CMS系统有哪些区别呢？现在市场上的企业网站的CMS系统几乎都没有考虑网站SEO的功能设置。U+CMS是由拥有10年开发经验的程序员和拥有10年网站优化经验的网络营销团队合作开发的内容管理系统，同时兼顾了企业网站的功能需求、程序的稳定性、安全性以及利于SEO的网站结构优势。</p><p>我们一直致力于产品的良好用户体验、网络营销效果而努力，我们力求每一个产品版本的发布，都要向前迈进，与时俱进。</p><p>2015年7月1日我们迎来了U+CMS内测版本发布的好消息。内测版本主要是针对中小企业网站建设及网站优化的解决方案。<br/>展望未来，我们将一如既往注重产品的用户体验和实用价值，为日新月异的互联网发展而不懈努力！<br/></p>',9999,0),(6,'联系我们','concatus',18,0,1,'联系我们',NULL,NULL,1,'company/concatus',NULL,NULL,'<p>联系电话：12345678910<br/></p>',9999,0),(7,'行业动态','hangye',18,4,2,'ucms-行业动态',NULL,NULL,2,NULL,'news/lister','news/detail',NULL,9999,0);
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_category` with 7 row(s)
--

--
-- Table structure for table `cms_contentimage`
--

DROP TABLE IF EXISTS `cms_contentimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_contentimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_contentimage`
--

LOCK TABLES `cms_contentimage` WRITE;
/*!40000 ALTER TABLE `cms_contentimage` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_contentimage` VALUES (4,5,3,'content/CzUo0zh2z33RA12Fuj8HSFt7nTSIj44kMwkzHGhe.jpeg'),(5,5,3,'content/IMVU5uvEpqiTbwk4a4Fc9L0Udj4ANbqr6qGoLYgd.jpeg'),(6,5,3,'content/vx0CkWgJMC7nn8V4T0RgZRU2RX1QLOh0OTbMkcVA.jpeg'),(7,2,4,'content/kmiZVRerqewnztAoaKkmKaY4C7wkkuOG0CuTos1U.jpeg'),(10,5,1,'content/zz6hmJqZ2J3TKhUPxKiag0usAU0yYrLKiJAcrDoB.jpeg'),(11,5,1,'content/Nmm0Wzbxzhhq0S10zBQ5tTu5CMq5fuxuh1KWEqiJ.jpeg');
/*!40000 ALTER TABLE `cms_contentimage` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_contentimage` with 6 row(s)
--

--
-- Table structure for table `cms_contentrelation`
--

DROP TABLE IF EXISTS `cms_contentrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_contentrelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `cidstr` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_contentrelation`
--

LOCK TABLES `cms_contentrelation` WRITE;
/*!40000 ALTER TABLE `cms_contentrelation` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_contentrelation` VALUES (2,5,3,'>1>3>'),(3,2,4,'>4>'),(4,3,4,'>4>'),(5,4,4,'>4>'),(6,5,4,'>4>'),(7,6,4,'>4>'),(8,7,4,'>4>'),(9,8,4,'>4>'),(10,9,4,'>4>'),(11,10,4,'>4>'),(12,11,7,'>4>7>'),(13,12,7,'>4>7>');
/*!40000 ALTER TABLE `cms_contentrelation` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_contentrelation` with 12 row(s)
--

--
-- Table structure for table `cms_friend`
--

DROP TABLE IF EXISTS `cms_friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(20) DEFAULT NULL,
  `flogo` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `furl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_friend`
--

LOCK TABLES `cms_friend` WRITE;
/*!40000 ALTER TABLE `cms_friend` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_friend` VALUES (1,'子沐博客','friend/itEv09RgaOIWfjw4NDaGAsWGgl6w1lTjAfS4Nqld.png',0,'https://zimu.website');
/*!40000 ALTER TABLE `cms_friend` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_friend` with 1 row(s)
--

--
-- Table structure for table `cms_modeldetail`
--

DROP TABLE IF EXISTS `cms_modeldetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_modeldetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(30) DEFAULT NULL,
  `cname` varchar(30) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `length` int(11) DEFAULT '0',
  `defaultvalue` varchar(255) DEFAULT '',
  `kvalue` varchar(255) DEFAULT NULL,
  `constraints` set('unsigned','unique','required','search','isshowonlist') DEFAULT NULL,
  `modelsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_modeldetail`
--

LOCK TABLES `cms_modeldetail` WRITE;
/*!40000 ALTER TABLE `cms_modeldetail` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_modeldetail` VALUES (97,'status','状态','tinyint',1,'','1-推荐@2-热销@3-置顶','isshowonlist',17),(98,'title','标题','varchar',255,'','','required,search,isshowonlist',17),(99,'content','简介','text',0,'','','required',17),(100,'price','市场价格','varchar',50,'','','',17),(101,'userprice','会员价格','varchar',50,'','','search,isshowonlist',17),(102,'style','规格','text',0,'','','',17),(103,'status','状态','tinyint',1,'','1-推荐@2-热销@3-置顶','isshowonlist',18),(104,'title','标题','varchar',255,'','','required,search,isshowonlist',18),(105,'content','内容','text',0,'','','required',18);
/*!40000 ALTER TABLE `cms_modeldetail` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_modeldetail` with 9 row(s)
--

--
-- Table structure for table `cms_models`
--

DROP TABLE IF EXISTS `cms_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namer` varchar(50) DEFAULT NULL,
  `tablename` varchar(50) DEFAULT NULL,
  `isimages` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `namer` (`namer`) USING BTREE,
  UNIQUE KEY `tablename` (`tablename`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_models`
--

LOCK TABLES `cms_models` WRITE;
/*!40000 ALTER TABLE `cms_models` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_models` VALUES (17,'产品','product',1),(18,'新闻','news',1);
/*!40000 ALTER TABLE `cms_models` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_models` with 2 row(s)
--

--
-- Table structure for table `cms_news`
--

DROP TABLE IF EXISTS `cms_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_news`
--

LOCK TABLES `cms_news` WRITE;
/*!40000 ALTER TABLE `cms_news` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_news` VALUES (2,1,'测试文章1-1','<p>内容-1<br/></p>'),(3,0,'fdsafdasd','<p>fdsadf<br/></p>'),(4,0,'fdsf','<p>fdsafd<br/></p>'),(5,0,'ttt1','<p>dfsafd<br/></p>'),(6,0,'ttt2','<p>fdsadf<br/></p>'),(7,0,'ttt3','<p>dfsadfsa<br/></p>'),(8,0,'ttt4','<p>dfasfdas<br/></p>'),(9,0,'ttt5','<p>fdasfsa<br/></p>'),(10,0,'ttt6','<p>dfsafda<br/></p>'),(11,0,'行业大发展','<p>abc<br/></p>'),(12,0,'测试测试行业动态','<p>的飞洒发<br/></p>');
/*!40000 ALTER TABLE `cms_news` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_news` with 11 row(s)
--

--
-- Table structure for table `cms_product`
--

DROP TABLE IF EXISTS `cms_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `price` varchar(50) DEFAULT NULL,
  `userprice` varchar(50) DEFAULT NULL,
  `style` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_product`
--

LOCK TABLES `cms_product` WRITE;
/*!40000 ALTER TABLE `cms_product` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_product` VALUES (5,1,'果壳净水炭1','<p>好东西</p>','100','99','<p>3斤/袋<br/></p>');
/*!40000 ALTER TABLE `cms_product` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_product` with 1 row(s)
--

--
-- Table structure for table `cms_roles`
--

DROP TABLE IF EXISTS `cms_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `power` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_roles`
--

LOCK TABLES `cms_roles` WRITE;
/*!40000 ALTER TABLE `cms_roles` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_roles` VALUES (1,'超级管理员','0|1|2|3|4|5|6|7|8'),(2,'前台管理员','2|3|4|6|7'),(3,'系统管理员','0|1|5'),(4,'用户管理员','8');
/*!40000 ALTER TABLE `cms_roles` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_roles` with 4 row(s)
--

--
-- Table structure for table `cms_roles_user`
--

DROP TABLE IF EXISTS `cms_roles_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_roles_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `roles_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_roles_user`
--

LOCK TABLES `cms_roles_user` WRITE;
/*!40000 ALTER TABLE `cms_roles_user` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_roles_user` VALUES (2,1,1),(5,6,2),(6,7,3),(7,7,4),(8,13,2),(9,13,3),(10,14,2),(11,14,3);
/*!40000 ALTER TABLE `cms_roles_user` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_roles_user` with 8 row(s)
--

--
-- Table structure for table `cms_site`
--

DROP TABLE IF EXISTS `cms_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitename` varchar(100) DEFAULT NULL,
  `indextemplate` varchar(50) DEFAULT NULL,
  `beian` varchar(100) DEFAULT NULL,
  `powerby` text,
  `keyword` text,
  `des` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_site`
--

LOCK TABLES `cms_site` WRITE;
/*!40000 ALTER TABLE `cms_site` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_site` VALUES (1,'ucms内容管理系统','index','备19020464','版权所有违者必究 Copyright © 2019-2021','cms,网站制作','制作企业站，内容管理站非常方便');
/*!40000 ALTER TABLE `cms_site` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_site` with 1 row(s)
--

--
-- Table structure for table `cms_user`
--

DROP TABLE IF EXISTS `cms_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL,
  `password` char(32) NOT NULL,
  `lastlogintime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_user`
--

LOCK TABLES `cms_user` WRITE;
/*!40000 ALTER TABLE `cms_user` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `cms_user` VALUES (1,'admin','123456',1619056372),(6,'fzj1','123456',1617846561),(7,'fzj2','123456',1617783961),(13,'fzj3','123456',1617864400),(14,'fzh2','123456',NULL);
/*!40000 ALTER TABLE `cms_user` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `cms_user` with 5 row(s)
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on: Thu, 22 Apr 2021 09:54:38 +0800
