<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */
// 是否是手机端测试
function isMobile()

{

        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备

    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))

    return true;



    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息

    if (isset ($_SERVER['HTTP_VIA']))

    {

    // 找不到为flase,否则为true

    return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;

    }

    // 脑残法，判断手机发送的客户端标志,兼容性有待提高

    if (isset ($_SERVER['HTTP_USER_AGENT']))

    {

        $clientkeywords = array ('nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile');

        // 从HTTP_USER_AGENT中查找手机浏览器的关键字

        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))

            return true;

    }

        // 协议法，因为有可能不准确，放到最后判断

    if (isset ($_SERVER['HTTP_ACCEPT']))

    {

    // 如果只支持wml并且不支持html那一定是移动设备

    // 如果支持wml和html但是wml在html之前则是移动设备

        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))

        {

            return true;

        }

    }

            return false;

 }
if(isMobile() && !preg_match("/\/mobile/i",$_SERVER['REQUEST_URI']))
{
    if($_SERVER['QUERY_STRING']==''){
        header("location:http://m.datiegeda.com/mobile/user/indexn");
    }else{
        header("location:http://m.datiegeda.com/mobile/user/indexn?".$_SERVER['QUERY_STRING']);
    }

	

	exit();

}
/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

require __DIR__.'/../bootstrap/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
