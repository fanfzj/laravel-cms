<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cname',20)->unique();
            $table->string('enname',50)->unique();
            $table->Integer('modelid');
            $table->Integer('pid');
            $table->tinyInteger('isshow')->default(1);
            $table->string('seotitle',255);
            $table->text('seokeyword');
            $table->tinyInteger('pagestyle')->default(1);
            $table->string('coverpath',255);
            $table->string('listpath',255);
            $table->string('detailpath',255);
            $table->text('content');
            $table->Integer('ordernum')->default(999);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
