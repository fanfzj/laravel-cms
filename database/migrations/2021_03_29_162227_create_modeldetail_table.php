<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModeldetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modeldetail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fieldname',30);
            $table->string('cname',30);
            $table->string('type',30);
            $table->Integer('length')->default(0);
            $table->string('defaultvalue',255)->default("");
            $table->string('kvalue',255);
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modeldetail');
    }
}
