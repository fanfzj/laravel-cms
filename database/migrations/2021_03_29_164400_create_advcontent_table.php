<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvcontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advcontent', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('cid');
            $table->Integer('advposid');
            $table->text('path');
            $table->text('content');
            $table->dateTime('createtime');
            $table->dateTime('updatetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advcontent');
    }
}
