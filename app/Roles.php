<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
  protected $table = "roles";
  public $timestamps = false;
  public $fillable = ['name', 'power'];
  public function user()
  {
    return $this->belongsToMany('App\User')->using('App\UserRoles');
  }
}
