<?php
namespace App\Providers;
 
use Illuminate\Support\ServiceProvider;
use App\Custom\CmsShowData;
 
class DataServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
 
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('CmsShowData',function(){
            //return new TestService();
            return new CmsShowData;
        });
    }
}