<?php 
namespace App\Custom;
 
use Illuminate\Support\Facades\Facade;
 
class CmsShowDataFacade extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'CmsShowData';
    }
}