<?php

namespace App\Custom;

use Illuminate\Support\Facades\DB;
use App\Category;
use App\Models;
use App\Advcontent;
use App\Friend;
use App\User;

class CmsShowData
{
	function getFriend($cid = '', $limit = [])
	{
		$ob = new Friend();
		if (!empty($cid)) {
			$ob = $ob->where('cid', $cid);
		}
		if ($limit) {
			$ob = $ob->offset($limit[0]);
			$ob = $ob->limit($limit[1]);
		}
		return $ob->orderBy('id', 'desc')->get();
	}
	//$where = ['age'=>['>'=>5]]
	function getCategory($where = [])
	{
		//category表
		$ob = new Category();
		if (!empty($where)) {
			foreach ($where as $key => $v) {
				if (is_array($v)) {
					$keys = array_keys($v);
					$ob = $ob->where($key, $keys[0], $v[$keys[0]]);
				} else {
					$ob = $ob->where($key, $v);
				}
			}
		}
		//获取栏目对应的模型名称，是否为封面  列表 pagestyle
		//url('news/list')
		$cols = $ob->select("category.*", "models.tablename")
			->join('models', 'category.modelid', '=', 'models.id')
			->get();
		$arr = [];
		foreach ($cols as $v) {
			$pagestyle = $v->pagestyle == 1 ? 'index' : 'list';
			$arr[] = ['id' => $v->id, 'tablename' => $v->tablename, 'pagestyle' => $pagestyle, 'cname' => $v->cname];
		}
		return $arr;
	}
	/**
	$limit = [0,5]
	$order = ['字段名','desc|asc']
	 **/
	function getContentList($tablename, $where = [], $limit = [], $order = [])
	{
		$classNamespace = "App\\" . ucfirst($tablename);
		$modedOb = Models::where('tablename', $tablename)->first();
		$modelid = $modedOb->id;
		$categoryCols = Category::where('modelid', $modelid)->get();
		$cateIdArr = [];
		foreach ($categoryCols as $v) {
			$cateIdArr[] = $v->id;
		}
		$ob = new $classNamespace();
		$ob = $ob->select($tablename . '.*', 'category.enname', 'contentrelation.cid')
			->leftJoin("contentrelation", $tablename . '.id', '=', 'contentrelation.contentid')
			->leftJoin('category', 'category.id', '=', 'contentrelation.cid')
			->whereIn('contentrelation.cid', $cateIdArr);
		if (!empty($where)) {
			foreach ($where as $key => $v) {
				if (is_array($v)) {
					$keys = array_keys($v);
					$ob = $ob->where($key, $keys[0], $v[$keys[0]]);
				} else {
					$ob = $ob->where($key, $v);
				}
			}
		}
		//处理limit
		if (!empty($limit)) {
			$ob = $ob->offset($limit[0]);
			$ob = $ob->limit($limit[1]);
		}
		//处理order
		if (!empty($order)) {
			$ob = $ob->orderBy($order[0], $order[1]);
		}
		$cols = $ob->get();
		$colsArr = [];
		foreach ($cols as $v) {
			//找图片
			$imageOb = DB::table('contentimage')->where('contentid', $v->id)
				->where('cid', $v->cid)->first();
			if ($imageOb) {
				$v->path = $imageOb->path;
			} else {
				$v->path = "default.jpg";
			}
			$colsArr[] = $v;
		}
		return $colsArr;
	}
	function getAdv($id)
	{
		$advOb = Advcontent::where('id', $id)->first();
		$content = $advOb->content;
		$path = trim($advOb->path, '|');
		if (!empty($path)) {
			$arr = explode('|', $path);
			foreach ($arr as $key => $v) {
				$content = str_replace('@imageSrc' . $key . "@", asset('upload') . '/' . $v, $content);
			}
		}
		return $content;
	}
	function getCategoryById($cid)
	{
		return Category::where('id', $cid)->first();
	}
	function query($sql)
	{
		$re = DB::statement($sql);
		var_dump($re);
		return [];
	}
	//获取根据角色对象提取角色id
	function getRolesList($roles)
	{
		$user_roles = array();
		foreach ($roles as $role) {
			array_push($user_roles, $role->id);
		}
		return $user_roles;
	}
}
