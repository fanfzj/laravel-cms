<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRoles extends Pivot
{
	protected $table = "roles_user";
	public $timestamps = false;
	public $incrementing = true;
	public $fillable = ['user_id', 'roles_id'];
}
