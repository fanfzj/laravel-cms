<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
class Advposition extends Model{
	protected $table = "advposition";
	public $timestamps = false;
	public $fillable = ['cid','namer'];
}