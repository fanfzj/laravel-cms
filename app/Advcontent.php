<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
class Advcontent extends Model{
	protected $table = "advcontent";
	public $timestamps = true;
	const CREATED_AT = "createtime";
	const UPDATED_AT = "updatetime";
	public $fillable = ['cid','advposid','content','path'];
	function getImagePath($path){
		$path = trim($path,'|');
		$arr = explode('|',$path);
		$reArr=[];
		foreach($arr as $v){
			$reArr[]=asset('upload')."/".$v;
		}
		return $reArr;
	}
}