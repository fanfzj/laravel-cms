<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $table = "user";
	public $timestamps = false;
	public $fillable = ['username', 'password', 'lastlogintime'];
	public function roles()
	{
		return $this->belongsToMany('App\Roles')->using('App\UserRoles');
	}
}
