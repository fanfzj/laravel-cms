<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models;
use Illuminate\Support\Facades\DB;

class HomeController
{
	//获取所有信息
	function all($tablename, $pagestyle, $cid)
	{
		//先判断$pagestyle
		if ($pagestyle == 'list') {
			//根据栏目id，获取栏目记录
			$cateOb = Category::where('id', $cid)->first();
			//获取列表数据，传给模板
			$classNamespace = "App\\" . ucfirst($tablename);
			$contentOb = new $classNamespace();
			$cols = $contentOb->select($tablename . '.*', "contentrelation.cid", "category.enname")
				->join('contentrelation', $tablename . ".id", '=', 'contentrelation.contentid')
				->join('category', 'category.id', '=', 'contentrelation.cid')
				->where('contentrelation.cidstr', 'like', '%>' . $cid . '>%')
				->orderBy($tablename . '.id', 'desc')
				->paginate(6);
			//每条记录配一个图片
			$imageArr = [];
			foreach ($cols as $v) {
				$imageOb = DB::table('contentimage')->where('contentid', $v->id)
					->where('cid', $v->cid)
					->first();
				if ($imageOb) {
					$imageArr[$v->id] = $imageOb->path;
				} else {
					$imageArr[$v->id] = 'default.jpg';
				}
			}
			$catestr = Category::idToNameStr($cateOb->id);
			//呈现列表页
			//列表页的模板
			return view($cateOb->listpath, ['cols' => $cols, 'imageArr' => $imageArr, 'tablename' => $tablename, 'cid' => $cid, 'cateOb' => $cateOb, 'catestr' => $catestr]);
		} elseif ($pagestyle == 'index') {
			//呈现封面页
			$cateOb = Category::where('id', $cid)->first();
			return view($cateOb->coverpath, ['cateOb' => $cateOb, 'cid' => $cid]);
		}
	}
	//搜索
	function search($keyword)
	{
		//根据栏目id，获取栏目记录
		$catelist = [];
		$collection = [];
		$imageArr = [];
		$classNamespace = "App\\" . ucfirst('models');
		$contentOb_ = new $classNamespace();
		$tables = $contentOb_->all();
		foreach ($tables as $t) {
			$tablename = $t->tablename;
			//获取列表数据，传给模板
			$classNamespace = "App\\" . ucfirst($tablename);
			$contentOb = new $classNamespace();
			$cols = $contentOb->select($tablename . '.*', "contentrelation.cid", "category.enname")
				->join('contentrelation', $tablename . ".id", '=', 'contentrelation.contentid')
				->join('category', 'category.id', '=', 'contentrelation.cid')
				->where($tablename . '.title', 'like', '%' . $keyword . '%')
				->orderBy($tablename . '.id', 'desc')->get();
			//每条记录配一个图片
			foreach ($cols as $v) {
				$imageOb = DB::table('contentimage')->where('contentid', $v->id)
					->where('cid', $v->cid)
					->first();
				if ($imageOb) {
					$imageArr[$v->id] = $imageOb->path;
				} else {
					$imageArr[$v->id] = 'default.jpg';
				}
			}
			array_push($catelist, $t->namer);
			array_push($collection, $cols);
		}
		//呈现列表页
		//列表页的模板
		return view('search', ['collection' => $collection, 'imageArr' => $imageArr, 'keyword' => $keyword, 'catelist' => $catelist]);
	}
	function detail($enname, $id)
	{
		//获取栏目信息
		$cateOb = Category::where('enname', $enname)->first();
		//读内容表
		$modelid = $cateOb->modelid;
		$mOb = Models::where('id', $modelid)->first();
		$tablename = $mOb->tablename;
		$classNamespace = "App\\" . ucfirst($tablename);
		$ob = new $classNamespace();
		$contentOb = $ob->where('id', $id)->first();
		//获取文章的栏目id
		$catestr = Category::idToNameStr($cateOb->id);
		//获取图片
		$imageOb = DB::table("contentimage")->where('contentid', $id)
			->where('cid', $cateOb->id)
			->first();
		//详细页
		return view($cateOb->detailpath, ['contentOb' => $contentOb, 'catestr' => $catestr, 'cateOb' => $cateOb, 'imageOb' => $imageOb]);
	}
}
