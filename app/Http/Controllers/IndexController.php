<?php 
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
class IndexController extends Controller{
	function index(){
		//获取站点信息
		$ob=DB::table('site')->first();
		$templateName = $ob->indextemplate;
		return view($templateName);
	}
}