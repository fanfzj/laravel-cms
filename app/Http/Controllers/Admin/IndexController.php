<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Category;
use App\Advcontent;
use App\Friend;

class IndexController
{
	function index()
	{
		//用户个数
		$user_num = User::count();
		//栏目个数
		$category_num = Category::count();
		//广告个数
		$adv_num = Advcontent::count();
		//友情链接个数
		$friend_num = Friend::count();
		// 栏目列表
		$category_arr = Category::getTree();
		return view('admin/index', ['user_num' => $user_num, 'category_num' => $category_num, 'adv_num' => $adv_num, 'friend_num' => $friend_num, 'category_arr' => $category_arr]);
	}
}
