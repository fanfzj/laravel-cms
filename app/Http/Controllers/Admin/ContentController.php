<?php 
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Modeldetail;
use App\Models;
use Illuminate\Support\Facades\DB;
class ContentController extends Controller{
	function index($cid=0){
		if($cid==0){
			//获取栏目表中第一个栏目
			$cateOb=Category::first();
			$cid=$cateOb->id;
		}
		//根据栏目id，获取模型id
		$cOb = Category::where("id",$cid)->first();
		$modelid = $cOb->modelid;
		//读modeldetail表，找到要显示的字段名字
		$mdCols = Modeldetail::where('modelsid',$modelid)
		->where('constraints','like',"%isshowonlist%")
		->get();
		//读当前内容表结构中status-->kvalue
		$statusOb = Modeldetail::where('fieldname','status')->where('modelsid',$modelid)->first();
		$kvalue = $statusOb->kvalue;
		$kvalueArr = explode('@',$kvalue);
		$newArr=['默认'];
		foreach($kvalueArr as $v){
			$kvalueArr1=explode('-',$v);
			$newArr[$kvalueArr1[0]]=$kvalueArr1[1];
		}
		$mdArr = $mdCols->toArray();
		//获取栏目下的内容 cms_news cms_product
		//根据模型id，获取模型记录 cms_models
		$mOb = Models::where('id',$modelid)->first();
		//栏目的模型  表名称
		$tablename = $mOb->tablename;
		$classNamespace = "App\\".ucfirst($tablename);
		$ob = new $classNamespace();//cms_news join cms_contentrelation
		$cols=$ob->select($tablename.".*")
		->join('contentrelation',$tablename.".id",'=','contentrelation.contentid')
		->where('cidstr','like',"%>$cid>%")
		->paginate(10);
		$cateArr = Category::getTree();
		return view('admin.content.index',['cid'=>$cid,'mdArr'=>$mdArr,'cols'=>$cols,'newArr'=>$newArr,'cateArr'=>$cateArr]);
	}
	function edit($id,$cid){
		if($cid==0){
			//获取栏目表中第一个栏目
			$cateOb=Category::first();
			$cid=$cateOb->id;
		}
		$arr = Category::getTree();
		//通过cid获取模型id
		$cateOb=Category::where('id',$cid)->first();
		//模型id
		$mid=$cateOb->modelid;
		//根据模id,获取模型记录
		$mOb=Models::where('id',$mid)->first();
		//根据模型id，获取内容表的字段 modeldetail
		$cols = Modeldetail::where('modelsid',$mid)->get();
		//内容在哪个表
		$tablename=$mOb->tablename;
		//根据内容的id，获取内容记录
		$classNamespace = "App\\".ucfirst($tablename);
		$contentOb = new $classNamespace();
		$ob=$contentOb->where('id',$id)->first();
		$rowArr = $ob->toArray();
		//获取当前内容的图片
		$imageCols=DB::table('contentimage')->where('contentid',$id)->where("cid",$cid)->get();
		return view('admin.content.edit',['cid'=>$cid,'cols'=>$cols,'arr'=>$arr,'mOb'=>$mOb,'rowArr'=>$rowArr,'imageCols'=>$imageCols]);
	}
	function doedit(Request $request){
		$arr = $request->all();
		$id = $arr['id'];
		$cid = $arr['cid'];
		unset($arr['_token']);
		unset($arr['cid']);
		unset($arr['id']);
		//处理图片
		$message = "没有上传图片";
		if(isset($arr['upload']) && !empty($arr['upload'])){
			//保存图片
			$num=0;
			foreach($arr['upload'] as $v){
				$path=$v->store('content','my');
				//写表contentimage
				DB::table('contentimage')->insert(['cid'=>$cid,'contentid'=>$id,'path'=>$path]);
				$num++;
			}
			$message = "上传{$num}张图片";
			unset($arr['upload']);
		}
		//存哪个内容表 news product
		$cateOb = Category::where('id',$cid)->first();
		$modelid = $cateOb->modelid;
		$modelOb = Models::where('id',$modelid)->first();
		$tablename = $modelOb->tablename;
		$classNamespace = "App\\".ucfirst($tablename);
		//保存内容记录
		$ob=new $classNamespace();
		$rows = $ob->where('id',$id)->update($arr);
		if($rows === false){
			$message.=",修改失败";
			return redirect()->back()->with('message',$message);
		}else{
			$message.=",修改成功";
			return redirect(url('admin/content/index',['cid'=>$cid]))->with('message',$message);
		}
	}
	function del($id,$cid){
		//通过cid,值内容表是谁
		$cateOb = Category::where('id',$cid)->first();
		$modelid= $cateOb->modelid;
		$modelOb = Models::where('id',$modelid)->first();
		$tablename = $modelOb->tablename;
		//实例化表对应的类
		$classNamespace = "App\\".ucfirst($tablename);
		$ob=new $classNamespace();
		DB::beginTransaction();
		$rows = $ob->where('id',$id)->delete();
		$imagePathArr=[];
		if($rows){
			//删除图片
			$cols=DB::table('contentimage')->where('contentid',$id)
			->where('cid',$cid)->get();
			$rows1=1;
			foreach($cols as $v){
				$imagePathArr[]='./upload/'.$v->path;
				$rows1 = DB::table('contentimage')->where('id',$v->id)->delete();
			}
			if($rows1){
				//删除内容栏目关系表数据
				$rows2 = DB::table('contentrelation')->where('contentid',$id)
					->where('cid',$cid)->delete();
				if($rows2){
					DB::commit();
					foreach($imagePathArr as $path){
						@unlink($path);
					}
					return redirect()->back()->with('message',"删除成功");
				}else{
					DB::rollback();
				}
			}else{
				DB::rollback();
			}
		}else{
			DB::rollback();
		}
		return redirect()->back()->with('message',"删除失败$rows1.$rows2");
	}
	function delimage(Request $request){
		$arr = $request->all();
		$id = $arr['id'];
		$path = $arr['path'];
		//删除记录contentimage
		$rows = DB::table('contentimage')->where('id',$id)->delete();
		if($rows){
			//删除图片
			@unlink("./upload/".$path);
			return response()->json(['result'=>'success','message'=>'删除图片成功']);
		}else{
			return response()->json(['result'=>'error','message'=>'删除图片失败']);
		}
	}
	function add($cid=0){
		if($cid==0){
			//获取栏目表中第一个栏目
			$cateOb=Category::first();
			$cid=$cateOb->id;
		}
		$arr = Category::getTree();
		//通过cid获取模型id
		$cateOb=Category::where('id',$cid)->first();
		//模型id
		$mid=$cateOb->modelid;
		//根据模id,获取模型记录
		$mOb=Models::where('id',$mid)->first();
		//根据模型id，获取内容表的字段 modeldetail
		$cols = Modeldetail::where('modelsid',$mid)->get();
		

		return view('admin.content.add',['cid'=>$cid,'cols'=>$cols,'arr'=>$arr,'mOb'=>$mOb]);
	}
	function doadd(Request $request){
		$arr = $request->all();
		//var_dump($arr);
		//根据栏目id，找到模型记录
		$cid = $arr['cid'];
		//根据栏目id,找模型id
		$cOb = Category::where("id",$cid)->first();
		$modelid = $cOb->modelid;
		//根据模型id，找模型记录
		$mOb = Models::where('id',$modelid)->first();
		//获取表名
		$tableName = $mOb->tablename;
		//内容表是什么？模型
		$modelStr = 'App\\'.ucfirst($tableName);
		$ob = new $modelStr();
		$re = $ob->create($arr);//写内容表
		if(is_object($re)){
			$contentid = $re->id;
			//图片表
			$message = "没有上传图片";
			if(isset($arr['upload']) && !empty($arr['upload'])){
				//有图片上传，保存图片
				$num = 0;
				foreach($arr['upload'] as $v){
					$path = $v->store('content','my');
					//写图片表
					DB::table('contentimage')->insert(['contentid'=>$contentid,'cid'=>$cid,'path'=>$path]);
					$num++;
				}
				$message = "有{$num}张图片上传";
			}
			//内容栏目关系表
			//产生当前栏目id对应父子关系串 >1>2>  >4>
			$cidStr = Category::idToIdStr($cid);
			DB::table('contentrelation')->insert([
				'contentid'=>$contentid,
    			'cid'=>$cid,
    			'cidstr'=>$cidStr
			]);
			$message .= ",记录保存成功";
			return redirect()->back()->with('message',$message);
		}else{
			return redirect()->back()->with('message',"内容添加失败");
		}
		
	}
}