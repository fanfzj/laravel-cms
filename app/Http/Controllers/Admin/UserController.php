<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Roles;

class UserController
{
	//登录
	function login()
	{
		return view('admin/user/login');
	}
	//登出
	function logout()
	{
		Session::flush();
		return redirect('admin/login');
	}
	//登录验证
	function check(Request $request)
	{
		$username = $request->get('username');
		$password = $request->get('password');
		$ob = User::where('username', $username)
			->where('password', $password)
			->first();
		if ($ob) {
			//创建session
			//获取当前用户power权限
			$power = array();
			$user_roles = User::find($ob->id)->roles;
			foreach ($user_roles as $role) {
				$power = array_merge($power, explode('|', $role->power));
			}
			Session::put(['username' => $username, 'adminid' => $ob->id, 'user_roles' => $user_roles, 'user_power' => array_unique($power)]);
			//更新此用户的登录时间
			$ob->lastlogintime = time();
			$ob->save();
			//跳转到后台的首页
			return redirect('admin')->with('message', "登录成功");
		} else {
			//跳转回登录页
			return redirect()->back()->with('message', "登录失败");
		}
	}
	//用户列表
	function index(Request $request)
	{
		$roles = Roles::all();
		$cols = User::orderBy('id', 'desc')->paginate(10);
		return view("admin.user.index", ['cols' => $cols, 'roles' => $roles]);
	}
	//增加用户
	function add()
	{
		$roles = Roles::all();
		return view('admin.user.add', ['roles' => $roles]);
	}
	function doadd(Request $request)
	{
		$arr = $request->all();
		$roles = $arr['roles'];
		unset($arr['roles']);
		$ob = User::create($arr);
		if (is_object($ob)) {
			$message = '添加成功';
			foreach ($roles as $roleId) {
				$ob->roles()->attach($roleId);
			}
		} else {
			$message = "添加失败";
		}
		return redirect('admin/user/index')->with('message', $message);
	}
	//根据id，修改用户信息
	function edit($id)
	{
		$roles = Roles::all();
		$ob = User::find($id);
		return view('admin/user/edit', ['ob' => $ob, 'roles' => $roles]);
	}
	function doedit(Request $request)
	{
		$arr = $request->all();
		$id = $arr['id'];
		// 判断是否存在roles字段
		if (isset($arr['roles'])) {
			$roles = $arr['roles'];
			unset($arr['roles']);
		} else {
			$roles = [];
		}
		unset($arr['_token']);
		$user = User::find($id);
		$user->roles()->sync($roles);
		$rows = $user->update($arr);
		$message = $rows === false ? '修改失败' : "修改成功";
		return redirect('admin/user/index')->with('message', $message);
	}
	//删除用户
	function del($id)
	{
		$user = User::find($id);
		$user->roles()->detach();
		$rows = $user->delete();
		$message = $rows ? "删除成功" : "删除失败";
		return redirect()->back()->with('message', $message);
	}
	//用户密码修改
	function edit_passwd()
	{
		$id = Session::get('adminid');
		$obj = User::find($id);
		return view('admin/user/passwd', ['ob' => $obj]);
	}
	//用户密码修改
	function doedit_passwd(Request $request)
	{
		$id = Session::get('adminid');
		$obj = User::find($id);
		$arr = $request->all();
		$oldpasswd = $arr['oldpassword'];
		unset($arr['_token']);
		$password = $arr['password'];
		if ($oldpasswd == $obj->password) {
			unset($arr['oldpassword']);
			$obj->password = $password;
			$rows = User::where('id', $id)->update($arr);
			$message = $rows === false ? '修改失败' : "修改成功";
		} else {
			$message = '原始密码错误';
		}
		$request->session()->put('message', $message);
		return  redirect('admin');
	}
}
