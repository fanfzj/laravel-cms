<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResoucesController extends Controller
{
	function index(Request $request)
	{
		$p = $request->get('p', '');
		if ($p == '') {
			$operdir = public_path();
			$backPath = public_path();
		} else {
			$operdir = $p;
			$backPath = preg_replace("/\/[^\/]*$/", '', $p);
		}
		$dir = opendir($operdir);
		$fileArr = [];
		$dirArr = [];
		while ($content = readdir($dir)) {
			if ($content != '.' && $content != '..') {
				//获取类 file dir
				$isfile = is_file($operdir . "/" . $content);
				$type =  $isfile ? 'file' : 'dir';
				if ($isfile) {
					$fileArr[] = ['path' => $content, 'type' => $type];
				} else {
					$dirArr[] = ['path' => $content, 'type' => $type];
				}
			}
		}
		return view('admin.resources.index', ['fileArr' => $fileArr, 'dirArr' => $dirArr, 'curdir' => $operdir, 'backPath' => $backPath]);
	}
	function upload(Request $request)
	{
		$curdir = $request->get('curdir');
		$filename = $request->get('filename');
		if ($filename == '') {
			return redirect()->back()->with('message', "请输入文件的名称");
		}
		$destinationPath = $curdir;
		$file = $request->file('upload');
		$re = $file->move($destinationPath, $filename);
		return redirect()->back()->with('message', "文件上传成功");
	}
	function del(Request $request)
	{
		$p = $request->get('p');
		@unlink($p);
		return redirect()->back()->with('message', "文件删除成功");
	}
}
