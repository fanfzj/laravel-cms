<?php 
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SetingController extends Controller{
	function edit(){
		//获取站点设置记录
		$ob = DB::table('site')->first();
		return view('admin/seting/edit',['ob'=>$ob]);
	}
	function doedit(Request $request){
		$arr = $request->all();
		unset($arr['_token']);
		if(!empty($arr['id'])){
			$id = $arr['id'];
			
			$re = DB::table('site')->where('id',$id)->update($arr);
		}else{
			unset($arr['id']);
			$re = DB::table('site')->insert($arr);
		}
		$message = $re ? "保存成功" : "保存失败";
		return redirect()->back()->with('message',$message);
	}
}