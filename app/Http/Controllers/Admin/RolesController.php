<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Admin;
use App\Roles;
use App\User;
use App\UserRoles;

class RolesController
{
  //角色列表
  function index(Request $request)
  {
    $cols = Roles::orderBy('id', 'asc')->paginate(10);
    return view("admin.roles.index", ['cols' => $cols]);
  }
  //增加角色
  function add()
  {
    return view('admin.roles.add');
  }
  function doadd(Request $request)
  {
    $arr = $request->all();
    $arr['power'] = implode('|', $arr['power']);
    $ob = Roles::create($arr);
    $message = is_object($ob) ? "添加成功" : "添加失败";
    return redirect('admin/roles/index')->with('message', $message);
  }
  //根据id，修改角色信息
  function edit($id)
  {
    $ob = Roles::find($id);
    return view('admin/roles/edit', ['ob' => $ob]);
  }
  function doedit(Request $request)
  {
    $arr = $request->all();
    $id = $arr['id'];
    unset($arr['_token']);
    $arr['power'] = implode('|', $arr['power']);
    $rows = Roles::where('id', $id)->update($arr);
    $message = $rows === false ? '修改失败' : "修改成功";
    return redirect('admin/roles/index')->with('message', $message);
  }
  //删除角色
  function del($id)
  {
    $roles = Roles::find($id);
    $row_ = $roles->user()->detach();
    $rows = $roles->delete();
    $message = $rows ? "删除成功" : "删除失败";
    return redirect()->back()->with('message', $message);
  }
}
