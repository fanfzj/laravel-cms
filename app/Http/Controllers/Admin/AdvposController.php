<?php 
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Category;
use App\Advposition;
use Illuminate\Http\Request;
class AdvposController extends Controller{
	function index(){
		//获取广告位数据
		$cols = Advposition::select("advposition.*",'category.cname')
		->leftJoin('category','advposition.cid','=','category.id')
		->paginate(8);
		return view("admin.advpos.index",['cols'=>$cols]);
	}
	function add(){
		$arr = Category::getTree();
		return view('admin.advpos.add',['arr'=>$arr]);
	}
	function doadd(Request $request){
		$arr = $request->all();
		$ob = Advposition::create($arr);
		$message = is_object($ob) ? "添加成功" : "添加失败";
		return redirect('admin/advpos/index')->with('message',$message);
	}
	function edit($id){
		$arr = Category::getTree();
		//根据id，获取广告位信息
		$ob = Advposition::find($id);

		return view('admin/advpos/edit',['ob'=>$ob,'arr'=>$arr]);
	}
	function doedit(Request $request){
		$arr = $request->all();
		$id = $arr['id'];
		unset($arr['_token']);
		$rows = Advposition::where('id',$id)->update($arr);
		$message = $rows===false ? '修改失败' : "修改成功";
		return redirect('admin/advpos/index')->with('message',$message);
	}
	function del($id){
		$rows = Advposition::where("id",$id)->delete();
		$message = $rows ? "删除成功" : "删除失败";
		return redirect()->back()->with('message',$message);
	}
}