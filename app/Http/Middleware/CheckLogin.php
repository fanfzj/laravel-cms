<?php

namespace App\Http\Middleware;

use App\Roles;
use App\User;
use Illuminate\Support\Facades\Session;
use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = array(
            'seting' => 0,
            'models' => 1,
            'user' => 8,
            'roles' => 8,
            'category' => 2,
            'content' => 3,
            'adv' => 4,
            'advpos' => 4,
            'data' => 5,
            'resources' => 6,
            'friend' => 7
        );
        if (Session::has('adminid')) {
            $path = $request->path();
            if ($path != 'admin' && $path != 'admin/user/password') {
                //忘记信息
                $route_get = explode('/', $path)[1];
                $roles = Session::get('user_power');
                if (in_array($route[$route_get], $roles)) {
                    return $next($request);
                } else {
                    return redirect('admin')->with('message', '无权限访问');
                }
            } else {
                return $next($request);
            }
        } else {
            return redirect('admin/login');
        }
    }
}
