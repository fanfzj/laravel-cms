<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>首页-{{ $site->sitename }}</title>
<meta name="keywords" content="{{ $site->keyword }}"/>
<meta name="description" content="{{ $site->des }}"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{{asset('home')}}/css/public.css" rel="stylesheet" type="text/css" />
<link href="{{asset('home')}}/css/header.css" rel="stylesheet" type="text/css" />
<link href="{{asset('home')}}/css/footer.css" rel="stylesheet" type="text/css" />
<link href="{{asset('home')}}/css/index.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--logo开始-->
@include('public/header')
<!--导航结束--> 
<!--幻灯开始-->
<div class="banner_index">
    <ul class="banner_wrap" id="banner_index">
      {!!Data::getAdv(4)!!}
    </ul>
	<div class="indexBanner_num" id="index_numIco"></div>
</div>
<script src="{{asset('home')}}/js/jquery-1.8.0.js" type="text/javascript"></script>
<script src="{{asset('home')}}/js/clothing_scroll.js" type="text/javascript"></script>
<script src="{{asset('home')}}/js/clothing_common.js" type="text/javascript"></script>
<script type="text/javascript">
var ShowPre1 = new ShowPre({box:"banner_index",Pre:"banner_index_pre",Next:"banner_index_next",numIco:"index_numIco",loop:1,auto:1});
</script>
<!-- 代码 结束 -->
<!--幻灯开始结束-->
</div>
<!--正文内容开始-->
	<!--行业资讯-->
	<div class="clothing_news_ablut"> 
	<!--行业资讯开始-->
	<div class="clothing_news fl">
		<div class="publictitle_whilt"><span>行业动态<b>type</b></span></div>
		
		<ul class="clothing_news_list">
			@foreach(Data::getContentList('news',['contentrelation.cid'=>'7'],[0,6],['id','desc']) as $v)
			<li @if($loop->last)style="border-bottom:0;"@endif><a href="{{url($v->enname.'/'.$v->id)}}">{{$v->title}}</a></li>
			@endforeach
					</ul>
	</div>
	<!--行业资讯结束--> 
	<!--关于开始-->
	<div class="clothing_about fr">
		<div class="publictitle_whilt"><span>关于我们<b>About</b></span></div>
		<div class="clothing_about_imgtxt"> <img src="{{asset('home')}}/images/tu02_44.jpg" width="158" height="86" alt="" />
			<p>{{mb_substr(strip_tags(Data::getCategoryById(5)->content),0,260)}}...</p>
		</div>
		</div>
	</div>
	<!--关于结束-->
	<div class="clear"></div>
</div>
<!--行业资讯结束-->
<!--产品--> 
<div class="clothing_product"> 
	<!--行业资讯开始-->
	<div class="fl">
		<div class="clothing_product_type">
		<div class="publictitle_whilt"><span>产品分类<b>type</b></span></div>
		<h2>
		<ul class="clothing_type_list">
			@foreach(Data::getCategory(['pid'=>1]) as $v)
			<li @if($loop->last)style="border-bottom:0;"@endif><a href="{{url('product_list_'.$v['id'])}}">{{$v['cname']}}</a>  >></li>
			@endforeach
		
		</ul>
		</h2>
		</div>
		<div class="clear"></div>
		<div style="border:1px solid #ddd;margin-top:10px;height:188px;"><img src="{{asset('home')}}/images/advus.jpg"/></div>
	</div>
	<!--行业资讯结束--> 
	<!--关于开始-->
		<div class="clothing_product_p fr"> 
		<div class="publictitle_whilt"><span>产品展示<b>product</b></span></div>
			
			<ul class="clothing_img">
				@foreach(Data::getContentList('product',['product.status'=>'1'],[0,4],['id','desc']) as $v)
			<li><a href="{{url($v->enname.'/'.$v->id)}}"><img src="{{asset('upload')}}/{{$v->path}}" width="194" height="200" alt="{{$v->title}}" />
					<p>{{$v->title}}</p>
					</a></li>
			@endforeach
			
								
			</ul>
			
			<div class="clear" style="height:10px;"></div>
	</div>
	<!--关于结束-->
	<div class="clear"></div>
</div>
<!--end-->
<!--文章资讯-->
<div class="clothing_san">
	<div class="clothing_san_left fl">
		<div class="publictitle_whilt"><span>推荐文章<b>Recommend</b></span></div>
		<ul class="clothing_san_list">
			@foreach(Data::getContentList('news',['news.status'=>'1'],[0,6],['id','desc']) as $v)
			<li @if($loop->last)style="border-bottom:0;"@endif><a href="{{url($v->enname.'/'.$v->id)}}">{{$v->title}}</a></li>
			@endforeach
		</ul>
	</div>
	<div class="clear"></div>
</div>
<!--文章资讯结束-->
<!--底部开始-->
@include("public.footer")
<!--底部结束-->
</body>
</html>