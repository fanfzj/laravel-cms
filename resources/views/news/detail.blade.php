@extends('layouts/index')
@section('title',$cateOb->seotitle)
@section('main')
<!--正文内容开始-->
<link href="{{asset('home')}}/css/newsDetail.css" rel="stylesheet" type="text/css" />
<!--产品--> 
<div class="content"> 
	<!--左侧开始-->
	<div class="fl">
		<div class="content_left">
		<div class="contenttitle_whilt"><span>最新新闻<b>news</b></span></div>
		
		<ul class="list">
			@foreach(Data::getContentList('news',[],[0,8],['id','desc']) as $v)
			<li @if($loop->last)style="border-bottom:0;"@endif><a href="{{url($v->enname.'/'.$v->id)}}">{{$v->title}}</a></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<div class="contenttitle_whilt"><span>产品分类<b>type</b></span></div>
		
		<ul class="list">
			@foreach(Data::getCategory(['category.pid'=>1]) as $v)
			<li @if($loop->last)style='border-bottom:0'@endif><a href="{{url($v['tablename'].'/'.$v['pagestyle'].'/'.$v['id'])}}">{{$v['cname']}}</a>&nbsp;&nbsp;>></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<img src="{{asset('home')}}/images/advus.jpg"/>
		</div>
	</div>
	<!--左侧结束--> 
	<!--关于开始-->
		<div class="content_right fr">
		<!--面包屑导航开始-->
		<div class="mbxdh">
		  <div class="mbxdh_left" style="width:100%"><a href="{{url('/')}}">首页</a> &gt;  {{$catestr}} &gt;  {{$contentOb->title}}</div>
		  <div class="clear"></div>
		</div>
		<!--面包屑导航结束-->
		<div class="content_bottom">  
		<div class="news">
		<h2>{{$contentOb->title}}</h2>
	<div class="author">
		<span>来源：</span>
	</div>
	</div>
    <div class="newsDetail">
	{!!$contentOb->content!!}
	</div>
		
		</div>
		<div class="clear"></div>
	</div>
	<!--关于结束-->
	<div class="clear"></div>
</div>
<!--end-->
@endsection