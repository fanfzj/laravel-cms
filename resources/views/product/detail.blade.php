@extends('layouts/index')
@section('title',$cateOb->seotitle)
@section('main')
<link href="{{asset('home')}}/css/productDetail.css" rel="stylesheet" type="text/css" />
<!--导航结束--> 
<!--正文内容开始-->
<!--产品--> 
<div class="content"> 
	<!--左侧开始-->
	<div class="fl">
		<div class="content_left">
		<div class="contenttitle_whilt"><span>最新新闻<b>news</b></span></div>
		
		<ul class="list">
			@foreach(Data::getContentList('news',[],[0,8],['id','desc']) as $v)
			<li @if($loop->last)style="border-bottom:0;"@endif><a href="{{url($v->enname.'/'.$v->id)}}">{{$v->title}}</a></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<div class="contenttitle_whilt"><span>产品分类<b>type</b></span></div>
		
		<ul class="list">
			@foreach(Data::getCategory(['category.pid'=>1]) as $v)
			<li @if($loop->last)style='border-bottom:0'@endif><a href="{{url($v['tablename'].'/'.$v['pagestyle'].'/'.$v['id'])}}">{{$v['cname']}}</a>&nbsp;&nbsp;>></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<img src="{{asset('home')}}/images/advus.jpg"/>
		</div>
	</div>
	<!--左侧结束--> 
	<!--关于开始-->
		<div class="content_right fr">
		<!--面包屑导航开始-->
		<div class="mbxdh">
		  <div class="mbxdh_left" style="width:100%"><a href="{{url('/')}}">首页</a> &gt;  {{$catestr}} &gt; {{$contentOb->title}}</div>
		  <div class="clear"></div>
		</div>
		<!--面包屑导航结束-->
		<div class="content_bottom">  
		<div class="product">
		<div class="dTop">
		<div class="img"><img src="{{asset('upload')}}/{{$imageOb->path}}" width="305"/></div>
		<div class="imgDetail">
			<div class="title"><h1>{{$contentOb->title}}</h1></div>
			<ul>
				<li><b>产品分类：</b><span>{{$cateOb->cname}}</span></li>
				<li><b>产品规格：</b><span>{{strip_tags($contentOb->style)}}</span></li>
				<li><b>产品编号：</b><span>p_{{$contentOb->id}}</span></li>
				<li><b>市场价格：</b><span>{{$contentOb->price}}元</span></li>
				<li><b>会员价格：</b><span>{{$contentOb->userprice}}元</span></li>
			</ul>
			<div class="btn" style="text-align:center;padding-top:18px;"><b>订购热线：</b><font style="font-size:30px;color:#990000;">400-000-000</font></div>
		</div>
		<div class="clear"></div>
		</div>
		<!-- chanpinjieshao -->
		<div class="pnav mt10" style="margin-right:3px;"><b>产品介绍：</b></div>
			<div class="productContent" style="text-indent:2em;"> 
			{!!$contentOb->content!!}
			</div>
		
		</div>
		<div class="product">
		<div class="pnav mt10" style="margin-right:3px;"><b>产品推荐：</b></div>
		<div class="productContent" style="padding-left:12px;">
			
			<ul class="clothing_img">
			@foreach(Data::getContentList('product',['product.status'=>'1'],[0,4],['id','desc']) as $v)
			<li><a href="{{url($v->enname.'/'.$v->id)}}"><img src="{{asset('upload')}}/{{$v->path}}" width="194" height="200" alt="{{$v->title}}" />
					<p>{{$v->title}}</p>
					</a></li>
			@endforeach
												
			</ul>
			</div>
		
		</div>
		</div>
		<div class="clear"></div>
	</div>
	<!--关于结束-->
	<div class="clear"></div>
</div>
<!--end-->
@endsection