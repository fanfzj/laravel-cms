<!DOCTYPE HTML>
<html>
<head>
<title>@yield('title')-{{ $site->sitename }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="{{ $site->keyword }}" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="{{asset('js/jquery-2.1.1.min.js')}}"></script> 
<!--icons-css-->
<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet"> 
<!--Google Fonts-->
<link href='https://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.config.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.all.min.js')}}"> </script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/lang/zh-cn/zh-cn.js')}}"></script>
</head>
<body>
<div class="page-container">	
   <div class="left-content" style="width:93.5%">
	   <div class="mother-grid-inner">
            <!--header start here-->
				<div class="header-main fixed" style="width: 97%;padding:.1em 3em .1em 1em;">
					<div class="header-left">
							<div class="logo-name">
									 <a href="{{url('admin')}}"> <h1>U<sub>+</sub>CMS</h1> 
									<!--<img id="logo" src="" alt="Logo"/>--> 
								  </a> 								
							</div>
							<!--search-box-->
								<div class="search-box">
									内容管理系统
								</div><!--//end-search-box-->
							<div class="clearfix"> </div>
						 </div>
						 <div class="header-right">
							<!--notification menu end -->
							<div class="profile_details">		
								<ul>
									<li class="dropdown profile_details_drop">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<div class="profile_img">	
												<span class="prfil-img"><img style="margin-top:8px;height:30px;" src="{{asset('images/header.png')}}" alt=""> </span> 
												<div class="user-name">
													<p>用户</p>
													<span>{{ Session::get('username') }}</span>
												</div>
												<i class="fa fa-angle-down lnr"></i>
												<i class="fa fa-angle-up lnr"></i>
												<div class="clearfix"></div>	
											</div>	
										</a>
										<ul class="dropdown-menu drp-mnu">
											<li> <a href="{{ url('admin/user/password') }}"><i class="fa fa-cog"></i> 修改密码</a> </li> 
											<li> <a href="{{ url('admin/logout') }}"><i class="fa fa-sign-out"></i> 退出</a> </li>
										</ul>
									</li>
								</ul>
							</div>
							<div class="clearfix"> </div>				
						</div>
				     <div class="clearfix"> </div>	
				</div>
<!--heder end here-->
<!--inner block start here-->
<div class="inner-block">
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}}</div>
@endif
@yield('main')
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
	 <p>{{ $site->powerby }}</p>
</div>	
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
    <div class="sidebar-menu" style="width:102px;position: fixed;">
		  	<div class="logo" style="left:25px;top:5px;"><a href="{{url('admin')}}"><img src='{{asset('images/logo.png')}}' style="width:50px"></a></div>		  
		    <div class="menu">
		      <ul id="menu">
						@if (in_array('0',Session::get('user_power')))
		        <li id="menu-home" ><a href="{{url('admin/seting')}}"><i class="glyphicon glyphicon-wrench" style="color:white;"></i><span>设置</span></a></li>
						@endif
						@if (in_array('1',Session::get('user_power')))
		        <li><a href="{{url('admin/models/index')}}"><i class="glyphicon glyphicon-compressed" style="color:white;"></i><span>模型</span></a></li>		
						@endif
						@if (in_array('2',Session::get('user_power')))
		        <li><a href="{{url('admin/category/index')}}"><i class="glyphicon glyphicon-th-list" style="color:white;"></i><span>栏目</span><span class="fa fa-angle-right" style="margin-top:-2px;float: right"></span></a>
		          <ul>
		            <li><a href="{{url('admin/category/add')}}">添加</a></li>
		            <li><a href="{{url('admin/category/index')}}">列表</a></li>		            
		          </ul>
		        </li>
						@endif
						@if (in_array('3',Session::get('user_power')))
		        <li><a href="{{url('admin/content/index')}}"><i class="glyphicon glyphicon-list-alt" style="color:white;"></i><span>内容</span><span class="fa fa-angle-right" style="margin-top:-2px;float: right"></span></a>
		          <ul>
		            <li><a href="{{url('admin/content/add')}}">添加</a></li>
		            <li><a href="{{url('admin/content/index')}}">列表</a></li>		            
		          </ul>
		        </li>
						@endif
						@if (in_array('4',Session::get('user_power')))
		        <li><a href="{{url('admin/adv/index')}}"><i class="glyphicon glyphicon-picture" style="color:white;"></i><span>广告</span><span class="fa fa-angle-right" style="margin-top:-2px;float: right"></span></a>
		          <ul>
		            <li><a href="{{url('admin/advpos/index')}}">广告位置</a></li>
		            <li><a href="{{url('admin/adv/index')}}">广告内容</a></li>		            
		          </ul>
		        </li>
						@endif
						@if (in_array('5',Session::get('user_power')))
		        <li><a href="{{url('admin/data/backup')}}"><i class="glyphicon glyphicon-duplicate" style="color:white;"></i><span>数据库</span><span class="fa fa-angle-right" style="margin-top:-2px;float: right"></span></a>
		          <ul>
		            <li><a href="{{url('admin/data/recovery')}}">备份</a></li>
		            <li><a href="{{url('admin/data/backup')}}">恢复</a></li>		            
		          </ul>
		        </li>
						@endif
						@if (in_array('6',Session::get('user_power')))
		        <li><a href="{{url('admin/resources/index')}}"><i class="glyphicon glyphicon-book" style="color:white;"></i><span>资源</span><span class="fa fa-angle-right" style="margin-top:-2px;float: right"></span></a>
		        </li>
						@endif
						@if (in_array('7',Session::get('user_power')))
		        <li><a href="{{url('admin/friend/index')}}"><i class="glyphicon glyphicon-envelope" style="color:white;"></i><span>友情链接</span><span class="fa fa-angle-right" style="margin-top:-2px;float: right"></span></a>
		          <ul>
		            <li><a href="{{url('admin/friend/index')}}">管理</a></li>
		            <li><a href="{{url('admin/friend/add')}}">添加</a></li>		            
		          </ul>
		        </li>
						@endif
						@if (in_array('8',Session::get('user_power')))
						<li>
							<a href="{{url('admin/user/index')}}"><i class=" glyphicon glyphicon-user" style="color:white;"></i><span>用户</span><span class="fa fa-angle-right" style="margin-top:-2px;float: right"></span></a>
								<ul>
									<li><a href="{{url('admin/user/index')}}">用户列表</a></li>		  
											<li><a href="{{url('admin/roles/index')}}">角色列表</a></li>          
								</ul>
							</li>
							@endif
		      </ul>
		    </div>
	 </div>
	<div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<!--scrolling js-->
		<script src="{{asset('js/jquery.nicescroll.js')}}"></script>
		<script src="{{asset('js/scripts.js')}}"></script>
		<!--//scrolling js-->
<script src="{{asset('js/bootstrap.js')}}"> </script>
<!-- mother grid end here-->
<script>
	$(function(){
        $(window).resize(function(){
		$('.left-content').css('width',($(document.body).width()-102)+'px')
			}).resize();
  })
	</script>
</body>
</html>                     