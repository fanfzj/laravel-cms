<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>@yield('title')-{{ $site->sitename }}</title>
<meta name="keywords" content="{{ $site->keyword }}"/>
<meta name="description" content="{{ $site->des }}"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{{asset('home')}}/css/public.css" rel="stylesheet" type="text/css" />
<link href="{{asset('home')}}/css/header.css" rel="stylesheet" type="text/css" />
<link href="{{asset('home')}}/css/footer.css" rel="stylesheet" type="text/css" />
<link href="{{asset('home')}}/css/index.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--logo开始-->
@include('public/header')
@yield('main')
<!--底部开始-->
@include("public.footer")
<!--底部结束-->
</body>
</html>