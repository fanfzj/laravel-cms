@extends('layouts/index')
@section('title',$keyword)
@section('main')
<link href="{{asset('home')}}/css/newsList.css" rel="stylesheet" type="text/css" />
<!--产品--> 
<div class="content"> 
	<!--左侧开始-->
	<div class="fl">
		<div class="content_left">
		<div class="contenttitle_whilt"><span>最新新闻<b>news</b></span></div>
		
		<ul class="list">
			@foreach(Data::getContentList('news',[],[0,8],['id','desc']) as $v)
			<li @if($loop->last)style="border-bottom:0;"@endif><a href="{{url($v->enname.'/'.$v->id)}}">{{$v->title}}</a></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<div class="contenttitle_whilt"><span>产品分类<b>type</b></span></div>
		
		<ul class="list">
			@foreach(Data::getCategory(['category.pid'=>1]) as $v)
			<li @if($loop->last)style='border-bottom:0'@endif><a href="{{url($v['tablename'].'/'.$v['pagestyle'].'/'.$v['id'])}}">{{$v['cname']}}</a>&nbsp;&nbsp;>></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<img src="{{asset('home')}}/images/advus.jpg"/>
		</div>
	</div>
	<!--左侧结束--> 
	<!--关于开始-->
		<div class="content_right fr">
      <h1><strong>搜索关键词：</strong> {{ $keyword }}</h1>
      @foreach ($collection as $cols)
		<!--面包屑导航开始-->
		<div class="mbxdh">
		  <div class="mbxdh_left"><a href="{{url('/')}}">首页</a>>&gt;  {{$catelist[$loop->index]}}</div>
		  <div class="clear"></div>
		</div>
		<!--面包屑导航结束-->
		<div class="content_bottom">  
        <div class="newslist">
            <ul>
              @foreach($cols as $v)
              <li class="li_in2">
                <h2>
									<a href="{{url($v->enname.'/'.$v->id)}}" title="{{$v->title}}">{{$v->title}}</a>
								</h2>
                <i><a href="{{url($v->enname.'/'.$v->id)}}">&gt;</a></i>
              </li>
              @endforeach
                <div class="clear"></div>
            </ul>
      </div>
		</div>
		<div class="clear"></div>
    @endforeach
		</div>
	<!--关于结束-->
	<div class="clear"></div>
</div>
<!--end-->
@endsection