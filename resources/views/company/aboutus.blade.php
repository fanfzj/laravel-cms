@extends('layouts/index')
@section('title',$cateOb->seotitle)
@section('main')
<link href="{{asset('home')}}/css/content.css" rel="stylesheet" type="text/css" />
<!--导航结束--> 
<!--正文内容开始-->
<!--产品--> 
<div class="content"> 
	<!--左侧开始-->
	<div class="fl">
		<div class="content_left">
		<div class="contenttitle_whilt"><span>最新新闻<b>news</b></span></div>
		
		<ul class="list">
			@foreach(Data::getContentList('news',[],[0,8],['id','desc']) as $v)
			<li @if($loop->last)style="border-bottom:0;"@endif><a href="{{url($v->enname.'/'.$v->id)}}">{{$v->title}}</a></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<div class="contenttitle_whilt"><span>产品分类<b>type</b></span></div>
		
		<ul class="list">
			@foreach(Data::getCategory(['category.pid'=>1]) as $v)
			<li @if($loop->last)style='border-bottom:0'@endif><a href="{{url($v['tablename'].'/'.$v['pagestyle'].'/'.$v['id'])}}">{{$v['cname']}}</a>&nbsp;&nbsp;>></li>
			@endforeach
					</ul>
		</div>
		<div class="content_left mt10">
		<img src="{{asset('home')}}/images/advus.jpg"/>
		</div>
	</div>
	<!--左侧结束--> 
	<!--关于开始-->
		<div class="content_right fr">
		<!--面包屑导航开始-->
		<div class="mbxdh">
		  <div class="mbxdh_left">关于我们</div>
		  <div class="clear"></div>
		</div>
		<!--面包屑导航结束-->
		<div class="content_bottom">  
		<div class="contenttitle_us"><span>关于我们</span></div>
		<div class="aboutUs">
		{!!$cateOb->content!!}
		</div>
		<div class="clear"></div>
		</div>
	</div>
	<!--关于结束-->
	<div class="clear"></div>
</div>
<!--end-->
@endsection