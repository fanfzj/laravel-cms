@extends('layouts/admin')
@section('title',"备份数据库")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/data/recovery')}}">数据库</a></li>
      <li class="active">备份</li>
    </ol>
  </div>
	<form class="form-horizontal" action="{{url('admin/data/dorecovery')}}" method="post">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">是否下载到本地：</label>
	    <div class="col-sm-5">
	      <input type="radio" name="isload" value="1" checked>否&nbsp;
	      <input type='radio' name='isload' value='2'>是
	    </div>
	    <span class="col-sm-4 help-block">请选择是否下载到本地</span>
	  </div>
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">默认编码：</label>
	    <div class="col-sm-5">
	    <select class="form-control" name="encode">
			  <option value="utf8">UTF-8</option>
		</select>
	    </div>
	    <span class="col-sm-4 help-block"></span>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">开始备份</button>
	    </div>
	  </div>
	  {{csrf_field()}}
	</form>
@endsection
