@extends('layouts/admin')
@section('title',"添加友情链接")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/friend/index')}}">友情链接</a></li>
      <li class="active">添加</li>
    </ol>
  </div>
	<form class="form-horizontal" action="{{url('admin/friend/doadd')}}" method="post" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">选择栏目：</label>
	    <div class="col-sm-5">
	      <select class="form-control" name="cid">
			  <option value='0'>首页</option>
			  @foreach($cateArr as $v)
			  <option value="{{$v['id']}}">{{$v['cnamestr']}}</option>
			  @endforeach
			</select>
	    </div>
	    <span class="col-sm-4 help-block">请选择栏目</span>
	  </div>
	  
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">上传图片：</label>
	    <div class="col-sm-5">
	      <input type="file" class="form-control" id="upload" name="upload">
	    </div>
	    <span class="col-sm-4 help-block"></span>
	  </div>
	  <input type="hidden" name="path" value="">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"></label>
	    <div class="col-sm-9">
	     <ul id="showImage">
	     	
	     </ul>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">名称：</label>
	    <div class="col-sm-5">
	      <input type="text"  class="form-control" name='fname' value="">
	    </div>
	    <span class="col-sm-5 help-block">请输入名称</span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">URL：</label>
	    <div class="col-sm-5">
	      <input type="text"  class="form-control" name='furl' value="">
	    </div>
	    <span class="col-sm-5 help-block">请输入URL</span>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">添加友情链接</button>
	    </div>
	  </div>
	  {{csrf_field()}}
	</form>
@endsection
