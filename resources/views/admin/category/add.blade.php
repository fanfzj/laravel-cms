@extends('layouts.admin')
@section('title',"添加栏目")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/category/index')}}">栏目</a></li>
      <li class="active">添加</li>
    </ol>
  </div>
  <ul id="myTab" class="nav nav-tabs" style="padding-left:160px;">
   <li class="active">
      <a href="#info" data-toggle="tab">
         基本信息
      </a>
   </li>
   <li><a href="#adv" data-toggle="tab">高级属性</a></li>
   <li><a href="#edu" data-toggle="tab">栏目介绍</a></li>
</ul>
<form class="form-horizontal" action="{{url('admin/category/doadd')}}" method="post">
<div id="myTabContent" class="tab-content">
   <div class="tab-pane fade in active" id="info">
      <div class="form-group">
	    <label class="col-sm-2 control-label">栏目名称：</label>
	    <div class="col-sm-5">
	      <input name='cname' type="text" value="{{old('cname')}}" class="form-control" id="inputEmail3" placeholder="栏目名称（中文）">
	    </div>
	    <span class="col-sm-5 help-block">{{$errors->first('cname')}}</span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">英文名称：</label>
	    <div class="col-sm-5">
	      <input name="enname" type="text" value="{{old('enname')}}" class="form-control" id="inputEmail3" placeholder="栏目的英文名称（拼音）">
	    </div>
	    <span class="col-sm-5 help-block">{{$errors->first('enname')}}</span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">模型：</label>
	    <div class="col-sm-5">
	      <select class="form-control" name="modelid">
	      	@foreach($modelCols as $v)
			  <option @if(old('modelid')==$v->id)selected='selected'@endif value="{{$v->id}}">{{$v->namer}}</option>
			@endforeach
			</select>
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">父栏目：</label>
	    <div class="col-sm-5">
	      	<select class="form-control" name="pid">
	      	 	<option value='0'>顶级栏目</option>
	      	 	@foreach($arr as $v)
	      	 	<option value='{{$v['id']}}'>{{$v['cnamestr']}}</option>
	      	 	@endforeach
			</select>
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">导航显示：</label>
	    <div class="col-sm-5">
	      <input type="radio" @if(old('isshow')==1 || empty(old('isshow')))checked="checked"@endif name='isshow'  value="1">显示&nbsp;<input type="radio" @if(old('isshow')==2)checked="checked"@endif name="isshow" value="2">不显示
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">优化标题：</label>
	    <div class="col-sm-5">
	      <textarea class="form-control" name='seotitle' rows="2">{{old('seotitle')}}</textarea>
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">Meta关键字：</label>
	    <div class="col-sm-5">
	      <textarea class="form-control" name='seokeyword' rows="2">{{old('seokeyword')}}</textarea>
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">Meta站点描述：</label>
	    <div class="col-sm-5">
	      <textarea class="form-control" name='seodes' rows="2" placeholder="优化站点描述">{{old('seodes')}}</textarea>
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
   </div>
   <div class="tab-pane fade" id="adv">
      <div class="form-group">
	    <label class="col-sm-2 control-label">页面属性：</label>
	    <div class="col-sm-5">
	      <input type="radio" @if(old('pagestyle')==1 || empty(old('pagestyle')))checked="checked"@endif value='1' name="pagestyle">封面
	      <input type="radio" value='2' @if(old('pagestyle')==2)checked="checked"@endif name="pagestyle">列表
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">封面模板：</label>
	    <div class="col-sm-5">
	      <input type="text"  name="coverpath" value="{{old('coverpath')}}" class="form-control" placeholder="封面模板">
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">列表模板：</label>
	    <div class="col-sm-5">
	      <input type="text" name="listpath" value="{{old('listpath')}}" class="form-control" placeholder="封面模板">
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">详细页模板：</label>
	    <div class="col-sm-5">
	      <input type="text" name="detailpath" value="{{old('detailpath')}}" class="form-control" placeholder="封面模板">
	    </div>
	    <span class="col-sm-5 help-block"></span>
	  </div>
   </div>
   <div class="tab-pane fade" id="edu">
      <div class="form-group">
	    <label class="col-sm-2 control-label">栏目介绍：</label>
	    <div class="col-sm-10">
	      <textarea id="content" name="content"  style="width:100%;height:200px;">{!!old('content')!!}</textarea>
	    </div>
	  </div>
   </div>
   
  </div>
  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	    	{{csrf_field()}}
	      <button type="submit" class="btn btn-success">添加栏目</button>
	    </div>
  </div>
</form>
<script type="text/javascript">
var ue = UE.getEditor('content');
</script>
@endsection