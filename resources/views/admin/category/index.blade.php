@extends('layouts.admin')
@section('title',"栏目管理")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/category/index')}}">栏目</a></li>
      <li class="active">列表</li>
    </ol>
  </div>
  <div class="oper">
    <a href="{{url('admin/category/add')}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>添加栏目</button></a>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <colgroup>
      	<col class="col-xs-1">
        <col class="col-xs-3">
        <col class="col-xs-1">
        <col class="col-xs-1">
        <col class="col-xs-2">
        <col class="col-xs-1">
        <col class="col-xs-3">
      </colgroup>
      <thead>
        <tr class="active">
          <th>ID</th>
          <th>栏目名称</th>
          <th>模型</th>
          <th>页面类型</th>
          <th>导航显示</th>
          <th  style="text-align: center;">内容</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($arr as $v)
        <tr>
          <td>{{$v['id']}}</td>
          <td>{{$v['cnamestr']}}</td>
          <td>{{$v['pagestyle']}}</td>
          <td>{{$v['modelname']}}</td>
          <td>{{$v['isshow']}}</td>
          <td style="text-align: center;"><a href="{{url('admin/content/index',['cid'=>$v['id']])}}">内容</a></td>
          <td>
            <a href="{{url('admin/category/edit',['id'=>$v['id']])}}"><button type="button" class="btn btn-warning" style="padding:1px 6px;">修改</button></a>
            <a href="javascript:void(0)" onclick="if(confirm('确认删除栏目吗？')){location.href='{{url('admin/category/del',['id'=>$v['id']])}}';}"><button type="button" class="btn btn-danger" style="padding:1px 6px;">删除</button></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection