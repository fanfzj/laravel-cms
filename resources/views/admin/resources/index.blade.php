@extends("layouts/admin")
@section('title',"管理静态资源")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li class="active">管理静态资源</li>
    </ol>
  </div>
  <div class="oper">
    <a href="{{url('admin/resources/index')}}?p={{$backPath}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>回到上一级</button></a>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <colgroup>
        <col class="col-xs-7">
        <col class="col-xs-2">
        <col class="col-xs-3">
      </colgroup>
      <thead>
        <tr class="active">
          <th>{{$curdir}}</th>
          <th>类型</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
       @foreach($dirArr as $v)
        <tr>
          <td><a href="{{url('admin/resources/index')}}?p={{str_replace('\\','/',$curdir)}}/{{$v['path']}}"><b>{{$v['path']}}</b></a></td>
          <td>{{$v['type']}}</td>
          <td>
            
          </td>
        </tr>
      @endforeach
       @foreach($fileArr as $v)
        <tr>
          <td>{{$v['path']}}</td>
          <td>{{$v['type']}}</td>
          <td>
            <a href="javascript:if(confirm('确认删除吗？')){ location.href='{{url('admin/resources/del')}}?p={{$curdir}}/{{$v['path']}}';}"><button type="button" class="btn btn-danger" style="padding:1px 6px;">删除</button></a>
          </td>
        </tr>
      @endforeach
      <tr>
        <td colspan="3">
          <form action="{{url('admin/resources/upload')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name='curdir' value="{{$curdir}}">
            <span  style="display: inline-block;">请选择文件：</span><input style="display: inline-block;" type="file" name='upload'>&nbsp;<input type='text' name='filename'>&nbsp;<input  style="display: inline-block;" type="submit" value="上传">
            {{csrf_field()}}
          </form>
        </td>
      </tr>
      </tbody>
    </table>
  </div>
@endsection