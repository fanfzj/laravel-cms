@extends('layouts.admin')
@section('title','添加内容')
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/content/index',['cid'=>$cid])}}">内容</a></li>
      <li class="active">添加</li>
    </ol>
  </div>
	<form class="form-horizontal" action="{{url('admin/content/doadd')}}" method="post" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-1 control-label">选择栏目</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="cid" onchange="changeUrl();">
	      	@foreach($arr as $v)
			  <option @if($v['id']==$cid)selected='selected'@endif value="{{$v['id']}}">{{$v['cnamestr']}}</option>
			 @endforeach
			</select>
	    </div>
	    <span class="col-sm-3 help-block"></span>
	  </div>
	  @if($mOb->isimages == 1)
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-1 control-label">图片</label>
	    <div class="col-sm-8">
	      <input type="file" name="upload[]" multiple="multiple"/>
	    </div>
	    <span class="col-sm-3 help-block"></span>
	  </div>
	  @endif
	  @foreach($cols as $v)
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-1 control-label">{{$v->cname}}：</label>
	    <div class="col-sm-8">
	      @if($v->type=='text')
	      <textarea style="width: 100%;height:150px;" name="{{$v->fieldname}}" id="{{$v->fieldname}}"></textarea>
	      @elseif($v->fieldname=='status')
	     <input checked="checked" type="radio" value="0" name="{{$v->fieldname}}">&nbsp;默认&nbsp;&nbsp;
	      @foreach($v->kvalueExplode($v->kvalue) as $kv)
	      <input type="radio" value="{{explode('-',$kv)[0]}}" name="{{$v->fieldname}}">{{explode('-',$kv)[1]}}&nbsp;&nbsp;
	      @endforeach
	      @else
	      <input type="text" name="{{$v->fieldname}}" class="form-control"  placeholder="{{$v->cname}}">
	      @endif
	    </div>
	    <span class="col-sm-3 help-block">
	   	@if(!empty($v->constraints))
		    @if(strpos($v->constraints,'required')||strpos($v->constraints,'unique'))
		    {{$errors->first($v->fieldname)}}
		    @endif
		@endif
	    </span>
	  </div>
	  @endforeach
	  
	  
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-11">
	      <button type="submit" class="btn btn-default">发布内容</button>
	      {{csrf_field()}}
	    </div>
	  </div>
	</form>
	<script type="text/javascript">
	function changeUrl(){
		
		//获取当前选中的option,value的值
		var cid=$("[name='cid']").val();
		location.href="{{url('admin/content/add')}}/"+cid;
	}
	@foreach($cols as $v)
	@if($v->type=='text')
    UE.getEditor('{{$v->fieldname}}');
    @endif
    @endforeach
</script>
@endsection