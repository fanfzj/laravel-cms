@extends('layouts.admin')
@section('title','修改内容')
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/content/index',['cid'=>$cid])}}">内容</a></li>
      <li class="active">修改</li>
    </ol>
  </div>
	<form class="form-horizontal" action="{{url('admin/content/doedit')}}" method="post" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-1 control-label">选择栏目</label>
	    <div class="col-sm-8">
	      <select class="form-control" disabled="disabled" name="cid">
	      	@foreach($arr as $v)
			  <option @if($v['id']==$cid)selected='selected'@endif value="{{$v['id']}}">{{$v['cnamestr']}}</option>
			 @endforeach
			</select>
	    </div>
	    <span class="col-sm-3 help-block"></span>
	  </div>
	  @if($mOb->isimages == 1)
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-1 control-label">图片</label>
	    <div class="col-sm-8">
	      <input type="file" name="upload[]" multiple="multiple"/>
	    </div>
	    <span class="col-sm-3 help-block"></span>
	  </div>
	  <div class="form-group">
	  	<label for="inputEmail3" class="col-sm-1 control-label"></label>
	    <div class="col-sm-11">
	    <style type="text/css">
	    .ul1{
	    	overflow: hidden;
	    }
	    .ul1 li{
	    	float:left;
	    	margin-right:8px;
	    }
	    .ul1 li a{
	    	display: block;
	    	text-align: center;
	    	height: 25px;
	    	line-height: 25p;
	    }
	    </style>
	    <ul class="ul1">
	    	@foreach($imageCols as $v)
	    	<li id="li_{{$v->id}}">
	    		<img width="50" src="{{asset('upload')}}/{{$v->path}}">
	    		<a href="javascript:void(0);" onclick="delImage('{{$v->id}}','{{$v->path}}')">删除</a>
	    	</li>
	    	@endforeach
	    </ul>
	    </div>
	  </div>
	  @endif
	  @foreach($cols as $v)
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-1 control-label">{{$v->cname}}：</label>
	    <div class="col-sm-8">
	      @if($v->type=='text')
	      <textarea style="width: 100%;height:150px;" name="{{$v->fieldname}}" id="{{$v->fieldname}}">{!!$rowArr[$v->fieldname]!!}</textarea>
	      @elseif($v->fieldname=='status')
	     <input @if($rowArr['status']==0) checked="checked" @endif type="radio" value="0" name="{{$v->fieldname}}">&nbsp;默认&nbsp;&nbsp;
	      @foreach($v->kvalueExplode($v->kvalue) as $kv)
	      <input @if($rowArr['status']==explode('-',$kv)[0]) checked="checked" @endif type="radio" value="{{explode('-',$kv)[0]}}" name="{{$v->fieldname}}">{{explode('-',$kv)[1]}}&nbsp;&nbsp;
	      @endforeach
	      @else
	      <input type="text" name="{{$v->fieldname}}" class="form-control" value="{{$rowArr[$v->fieldname]}}"  placeholder="{{$v->cname}}">
	      @endif
	    </div>
	    <span class="col-sm-3 help-block">
	   	@if(!empty($v->constraints))
		    @if(strpos($v->constraints,'required')||strpos($v->constraints,'unique'))
		    {{$errors->first($v->fieldname)}}
		    @endif
		@endif
	    </span>
	  </div>
	  @endforeach
	  
	  
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-11">
	      <button type="submit" class="btn btn-default">发布内容</button>
	      <input type="hidden" name="id" value="{{$rowArr['id']}}" />
	      <input type="hidden" name="cid" value="{{$cid}}" />
	      {{csrf_field()}}
	    </div>
	  </div>
	</form>
	<script type="text/javascript">
	function changeUrl(){
		
		//获取当前选中的option,value的值
		var cid=$("[name='cid']").val();
		location.href="{{url('admin/content/add')}}/"+cid;
	}
	function delImage(id,path){
		$.ajax({
			url:'{{url("admin/content/delimage")}}',
			data:'id='+id+'&path='+path,
			type:'get',
			dataType:'json',
			success:function(re){
				if(re.result == 'success'){
					//删除对应li
					$("#li_"+id).remove();
				}else{
					alert(re.message);
				}
			}
		})
	}
	@foreach($cols as $v)
	@if($v->type=='text')
    UE.getEditor('{{$v->fieldname}}');
    @endif
    @endforeach
</script>
@endsection