@extends('layouts.admin')
@section('title',"管理内容")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/content/index',['cid'=>$cid])}}">内容</a></li>
      <li class="active">列表</li>
    </ol>
  </div>
  <div class="oper col-sm-9">
    <a href="{{url('admin/content/add',['cid'=>$cid])}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>添加内容</button></a>
  </div>
  <div class="col-sm-3">
    <select class="form-control" name="cid" onchange="changeUrl();">
      @foreach($cateArr as $v)
        <option @if($v['id']==$cid)selected='selected'@endif value="{{$v['id']}}">{{$v['cnamestr']}}</option>
      @endforeach
    </select>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <thead>
        <tr class="active">
          @foreach($mdArr as $v)
          @if($v['fieldname'] != 'status')
            <th>{{$v['cname']}}</th>
          @endif
          @endforeach
          @foreach($mdArr as $v)
          @if($v['fieldname'] == 'status')
            <th>{{$v['cname']}}</th>
          @endif
          @endforeach
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        @foreach($cols as $v)
        <tr>
          @foreach($mdArr as $vm)
          @if($vm['fieldname'] != 'status')
          <td>{{$v[$vm['fieldname']]}}</td>
          @endif
          @endforeach
          @foreach($mdArr as $vm)
          @if($vm['fieldname'] == 'status')
          <td>{{$newArr[$v[$vm['fieldname']]]}}</td>
          @endif
          @endforeach
          <td>
            <a href="{{url('admin/content/edit',['id'=>$v['id'],'cid'=>$cid])}}"><button type="button" class="btn btn-warning" style="padding:1px 6px;">修改</button></a>
            <a href="javascript:if(confirm('您确认删除吗?')){ location.href='{{url('admin/content/del',['id'=>$v['id'],'cid'=>$cid])}}'};"><button type="button" class="btn btn-danger" style="padding:1px 6px;">删除</button></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="listPage">
    {{$cols->appends($_GET)->links()}}
  </div>
  <script type="text/javascript">
    function changeUrl(){
    //获取当前选中的option,value的值
    var cid=$("[name='cid']").val();
    location.href="{{url('admin/content/index')}}/"+cid;
  }
  </script>
@endsection