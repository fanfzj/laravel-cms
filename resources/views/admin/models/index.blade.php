@extends('layouts.admin')
@section('title','模型管理')
@section('main')
  <div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/models/index')}}">模型管理</a></li>
    </ol>
  </div>
  <div class="oper">
    <a href="{{url('admin/models/add')}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>添加模型</button></a>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-1">
        <col class="col-xs-7">
        <col class="col-xs-3">
      </colgroup>
      <thead>
        <tr class="active">
          <th>名称</th>
          <th>表名</th>
          <th>图片</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        @foreach($cols as $v)
        <tr>
          <td>{{$v->namer}}</td>
          <td>{{$v->tablename}}</td>
          <td>@if($v->isimages==1)有@else无@endif</td>
          <td>
            <a class="btn btn-warning" href="{{url('admin/models/edit',['id'=>$v->id])}}" style="padding:1px 6px;">修改</a>
            <a href="javascript:if(confirm('确认删除吗？')){ location.href='{{url('admin/models/del',['id'=>$v->id])}}';}" class="btn btn-danger" style="padding:1px 6px;">删除</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="listPage">
    {{$cols->appends($_GET)->links()}}
  </div>
@endsection