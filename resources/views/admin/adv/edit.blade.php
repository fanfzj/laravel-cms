@extends('layouts/admin')
@section('title',"修改广告内容")
@section('main')
<script type="text/javascript" src="{{asset('js/ajaxfileupload.js')}}"></script>
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/adv/index')}}">广告</a></li>
      <li class="active">修改</li>
    </ol>
  </div>
	<form class="form-horizontal" action="{{url('admin/adv/doedit')}}" method="post">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">选择栏目：</label>
	    <div class="col-sm-5">
	      <select class="form-control" name="cid" onchange="getAdvpos();">
			  <option value='0'>首页</option>
			  @foreach($cateArr as $v)
			  <option @if($v['id'] == $ob->cid) selected @endif value="{{$v['id']}}">{{$v['cnamestr']}}</option>
			  @endforeach
			</select>
	    </div>
	    <span class="col-sm-4 help-block">请选择栏目</span>
	  </div>
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">选择广告位：</label>
	    <div class="col-sm-5">
	      <select class="form-control" name="advposid">
			  @foreach($advposCols as $v)
			  <option @if($v->id == $ob->advposid) selected @endif value="{{$v->id}}">{{$v->namer}}</option>
			  @endforeach
			</select>
	    </div>
	    <span class="col-sm-4 help-block">请选择广告位</span>
	  </div>
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">上传图片：</label>
	    <div class="col-sm-5">
	      <input onchange="changImg();" type="file" class="form-control" id="upload" name="upload">
	    </div>
	    <span class="col-sm-4 help-block"></span>
	  </div>
	  <input type="hidden" name="path" value="{{$ob->path}}">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"></label>
	    <div class="col-sm-9">
	     <ul id="showImage">
	     	@foreach($ob->getImagePath($ob->path) as $v)
	     	<li id="li_{{preg_replace('/\.|\//','',preg_replace('/.*?\/upload\//','',$v))}}">
	     		<img src="{{$v}}" height="80">
				<a href="javascript:delImage('{{preg_replace("/.*?\/upload\//",'',$v)}}')">删除</a>
	     	</li>
	     	@endforeach
	     </ul>
	    </div>
	  </div>
	  <style type="text/css">
	  	#showImage{
	  		list-style: none;
	  		overflow: hidden;
	  	}
	  	#showImage li{
	  		float: left;
	  		margin-right: 8px;
	  		height:108px;
	  	}
	  	#showImage li a{
	  		display: block;
	  		height:28px;
	  		line-height: 28px;
	  		text-align: center;
	  	}
	  </style>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">广告HTML内容：</label>
	    <div class="col-sm-5">
	      <textarea name="content" class="form-control" rows="5">{{$ob->content}}</textarea>
	    </div>
	    <span class="col-sm-5 help-block">请输入广告HTML内容</span>
	  </div>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">修改广告</button>
	    </div>
	  </div>
	  <input type="hidden" name="id" value="{{$ob->id}}">
	  {{csrf_field()}}
	</form>
<script type="text/javascript">
function getAdvpos(){
	//获取被选中的栏目id
	var cid = $("[name='cid']").val();
	//ajax
	$.ajax({
		url:'{{url("admin/adv/getadvpos")}}',
		data:'cid='+cid,
		dataType:'json',
		success:function(re){
			$("[name='advposid']").empty();
			for(var i in re){
				$("[name='advposid']").append("<option value='"+re[i].id+"'>"+re[i].namer+"</option>")
			}
		}
	})
}
var path="|";//|a.jpg|b.jpg|c.jpg|
function changImg(){
    $.ajaxFileUpload
      (
         {
              url:"{{url('api/adv/upload')}}", //上传文件的服务端
              secureuri:false,  //是否启用安全提交
              dataType: 'json',   //数据类型
              fileElementId:'upload', //表示文件域ID
              //提交成功后处理函数      html为返回值，status为执行的状态
              success: function(re,status)  
              {
              	if(re.result == 'success'){
              		var imagePath = re.path;
              		var idPath = imagePath.replace(/\/|\./g,'');
              		
              		//处理隐藏域path
              		path = $("[name='path']").val();
              		if(path == ''){path='|';}
              		path+=re.path+"|";
              		$("[name='path']").val(path);
              		$("#showImage").append("<li id='li_"+idPath+"'><img height='80' src='{{asset('upload')}}/"+re.path+"'><a href='javascript:delImage(\""+re.path+"\")'>删除</a></li>");
              	}else{
              		alert(re.message);
              	}
              },
              //提交失败处理函数
              error: function (re,status,e)
              {
                 alert(2);
              }
       }
   )

 }
 function delImage(path){
 	$.ajax({
 		url:'{{url("admin/adv/delimage")}}',
 		type:'get',
 		data:'path='+path,
 		success:function(re){
 			var idPath = path.replace(/\/|\./g,'');
 			if(re.result == 'success'){
 				var hiddenPath = $("[name='path']").val();
 				$("[name='path']").val(hiddenPath.replace(path+'|',''));
 				$("#li_"+idPath).remove();
 			}else{
 				alert(re.message);
 			}
 		}
 	})
 }
</script>
@endsection
