@extends("layouts/admin")
@section('title',"管理广告内容")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/adv/index')}}">广告</a></li>
      <li class="active">列表</li>
    </ol>
  </div>
  <div class="oper">
    <a href="{{url('admin/adv/add')}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>添加广告</button></a>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-2">
        <col class="col-xs-5">
        <col class="col-xs-1">
        <col class="col-xs-3">
      </colgroup>
      <thead>
        <tr class="active">
          <th>id</th>
          <th>广告位名称</th>
          <th>广告图片</th>
          <th>栏目</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        @foreach($cols as $v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->namer}}</td>
          <td>
            @foreach($v->getImagePath($v->path) as $vn)
            <img src="{{$vn}}" width="50">
            @endforeach
          </td>
          <td>@if($v->cid==0)首页@else {{$v->cname}} @endif</td>
          <td>
            <a href="{{url('admin/adv/edit',['id'=>$v->id])}}"><button type="button" class="btn btn-warning" style="padding:1px 6px;">修改</button></a>
            <a href="javascript:if(confirm('确认删除吗？')){ location.href='{{url('admin/adv/del',['id'=>$v->id])}}';}"><button type="button" class="btn btn-danger" style="padding:1px 6px;">删除</button></a>
          </td>
        </tr>
        @endforeach
        
      </tbody>
    </table>
  </div>
  <div class="listPage">
    {{$cols->links()}}
  </div>
@endsection