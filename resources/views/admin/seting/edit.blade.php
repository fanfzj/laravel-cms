@extends('layouts/admin')
@section('title',"网站设置")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li>设置</li>
    </ol>
  </div>

<form class="form-horizontal" action="{{url('admin/seting/doedit')}}" method="post">
   <div class="tab-pane fade in active" id="info">
      
    <div class="form-group">
      <label class="col-sm-2 control-label">网站名称：</label>
      <div class="col-sm-5">
        <input type="text" value="{{$ob->sitename}}" class="form-control" name='sitename' placeholder="站点名称">
      </div>
      <span class="col-sm-5 help-block">请输入网站名称</span>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">首页模板：</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="indextemplate" value="{{$ob->indextemplate}}" placeholder="首页模板路径" value="index">
      </div>
      <span class="col-sm-5 help-block">请输入首页模板</span>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">网站备案号：</label>
      <div class="col-sm-5">
        <input type="text"  value="{{$ob->beian}}" class="form-control" name="beian" placeholder="网站备案号">
      </div>
      <span class="col-sm-5 help-block">请输入网站备案号</span>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">网站版权信息：</label>
      <div class="col-sm-5">
        <textarea class="form-control" name="powerby" rows="2">{{$ob->powerby}}</textarea>
      </div>
      <span class="col-sm-5 help-block">请输入网站版权信息</span>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Meta关键字：</label>
      <div class="col-sm-5">
        <textarea name="keyword" class="form-control" rows="2">{{$ob->keyword}}</textarea>
      </div>
      <span class="col-sm-5 help-block">请输入Meta关键字</span>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Meta站点描述：</label>
      <div class="col-sm-5">
        <textarea name="des" class="form-control" rows="2" placeholder="优化站点描述">{{$ob->des}}</textarea>
      </div>
      <span class="col-sm-5 help-block">请输入Meta站点描述</span>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-info">保存设置</button>
        {{csrf_field()}}
        <input type="hidden" name="id" value="{{$ob->id}}">
      </div>
  </div>
   </div>
    
</form>
@endsection