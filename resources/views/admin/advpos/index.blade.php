@extends('layouts.admin')
@section('title','广告位列表')
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/advpos/index')}}">广告位</a></li>
      <li class="active">列表</li>
    </ol>
  </div>
<div class="oper">
    <a href="{{url('admin/advpos/add')}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>添加广告位</button></a>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-3">
        <col class="col-xs-5">
        <col class="col-xs-3">
      </colgroup>
      <thead>
        <tr class="active">
          <th>id</th>
          <th>广告位名称</th>
          <th>栏目</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        @foreach($cols as $v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->namer}}</td>
          <td>@if($v->cid == 0) 首页 @else {{$v->cname}} @endif</td>
          <td>
            <a href="{{url('admin/advpos/edit',['id'=>$v->id])}}"><button type="button" class="btn btn-warning" style="padding:1px 6px;">修改</button></a>
            <a href="javascript:if(confirm('确认是否删除')){location.href='{{url("admin/advpos/del",['id'=>$v->id])}}'};"><button type="button" class="btn btn-danger" style="padding:1px 6px;">删除</button></a>
          </td>
        </tr>
        @endforeach
        
      </tbody>
    </table>
  </div>
  <div class="listPage">
    {{$cols->links()}}
  </div>
@endsection