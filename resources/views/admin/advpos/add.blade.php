@extends('layouts.admin')
@section('title','广告位列表')
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/advpos/index')}}">广告位</a></li>
      <li class="active">添加</li>
    </ol>
  </div>
  <form class="form-horizontal" action="{{url('admin/advpos/doadd')}}" method="post">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">选择栏目</label>
	    <div class="col-sm-5">
	    <select class="form-control" name="cid">
	    <option value="0">首页</option>
		@foreach($arr as $v)	
		<option value="{{$v['id']}}">{{$v['cnamestr']}}</option>
		@endforeach  
		</select>
	    </div>
	    <span class="col-sm-4 help-block">请选择栏目</span>
	  </div>
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">广告位名称：</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" name="namer" placeholder="广告位名称">
	    </div>
	    <span class="col-sm-4 help-block">请输入广告位名称</span>
	  </div>
	  
	  
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-11">
	      <button type="submit" class="btn btn-default">添加广告位</button>
	      {{csrf_field()}}
	    </div>
	  </div>
	</form>
@endsection