@extends('layouts.admin')
@section('title','广告位修改')
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/advpos/index')}}">广告位</a></li>
      <li class="active">修改</li>
    </ol>
  </div>
  <form class="form-horizontal" action="{{url('admin/advpos/doedit')}}" method="post">
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">选择栏目</label>
	    <div class="col-sm-5">
	    <select class="form-control" name="cid">
	    <option value="0" @if($ob->cid == 0) selected @endif>首页</option>
		@foreach($arr as $v)	
		<option @if($v['id']==$ob->cid) selected @endif value="{{$v['id']}}">{{$v['cnamestr']}}</option>
		@endforeach  
		</select>
	    </div>
	    <span class="col-sm-4 help-block">请选择类栏目</span>
	  </div>
	  <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">广告位名称：</label>
	    <div class="col-sm-5">
	      <input value="{{$ob->namer}}" type="text" class="form-control" name="namer" placeholder="广告位名称">
	    </div>
	    <span class="col-sm-4 help-block">请输入广告位名称</span>
	  </div>
	  
	  
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-11">
	      <button type="submit" class="btn btn-default">修改广告位</button>
	      {{csrf_field()}}
	      <input type="hidden" name="id" value="{{$ob->id}}">
	    </div>
	  </div>
	</form>
@endsection