@extends('layouts.admin')
@section('title',"修改角色")
@section('main')
	<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/roles/index')}}">角色</a></li>
      <li class="active">修改</li>
    </ol>
  	</div>
  	<div class="alert alert-success alert-dismissable hide">
            <button type="button" class="close" onclick="$('.alert').addClass('hide')">
                &times;
            </button>
            <b></b>
    </div>
		<form class="form-horizontal" id="form1" method="post" action="{{ url('admin/roles/doedit') }}">
			<input type="hidden" name="id" value="{{$ob->id}}">
			<div class="form-group">
				<label for="username" class="col-sm-1 control-label">角色名：</label>
				<div class="col-sm-6">
					<input type="text" name="name" class="form-control" id="name" placeholder="请填写角色名" value="{{ $ob->name }}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-1 control-label">权限：</label>
				<div class="col-sm-11">
					<label class="checkbox-inline"><input type="checkbox" name="power[]" value="0" 
						@if (in_array('0',explode("|",$ob->power))) checked @endif />&nbsp;系统设置&nbsp;</label>
					<label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="1"
						@if (in_array('1',explode("|",$ob->power))) checked @endif />&nbsp;模型管理&nbsp;</label>
					<label class="checkbox-inline"><input type="checkbox" name="power[]" value="2"
						@if (in_array('2',explode("|",$ob->power))) checked @endif />&nbsp;栏目管理&nbsp;</label>
					<label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="3"
						@if (in_array('3',explode("|",$ob->power))) checked @endif />&nbsp;内容管理&nbsp;</label>
					<label class="checkbox-inline"><input type="checkbox" name="power[]" value="4"
						@if (in_array('4',explode("|",$ob->power))) checked @endif />&nbsp;广告管理&nbsp;</label>
					<label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="5"
						@if (in_array('5',explode("|",$ob->power))) checked @endif />&nbsp;数据库管理&nbsp;</label>
					<label class="checkbox-inline"><input type="checkbox" name="power[]" value="6"
						@if (in_array('6',explode("|",$ob->power))) checked @endif />&nbsp;资源管理&nbsp;</label>
					<label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="7"
						@if (in_array('7',explode("|",$ob->power))) checked @endif />&nbsp;友情链接管理&nbsp;</label>
					<label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="8"
						@if (in_array('8',explode("|",$ob->power))) checked @endif />&nbsp;用户管理&nbsp;</label>
				</div>
			</div>
		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-11">
				<button type="submit" style="margin:30px 15px 0 0;" class="btn btn-danger dropdown-toggle" id="addRowbtn">
				<span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>保存
			</button>
			</div>
	</div>
	{{csrf_field()}}
</form>
@endsection                