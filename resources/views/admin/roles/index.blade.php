@extends('layouts.admin')
@section('title','角色管理')
@section('main')
  <div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/roles/index')}}">角色管理</a></li>
    </ol>
  </div>
  <div class="oper">
    <a href="{{url('admin/roles/add')}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>添加角色</button></a>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-9">
        <col class="col-xs-2">
      </colgroup>
      <thead>
        <tr class="active">
          <th>角色名</th>
          <th>角色权限</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        @foreach($cols as $v)
        <tr>
          <td>{{ $v->name }}</td>
            <td>
              <label class="checkbox-inline"><input type="checkbox" name="power[]" value="0" disabled
                @if (in_array('0',explode("|",$v->power))) checked @endif />&nbsp;系统设置&nbsp;</label>
              <label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="1" disabled
                @if (in_array('1',explode("|",$v->power))) checked @endif />&nbsp;模型管理&nbsp;</label>
              <label class="checkbox-inline"><input type="checkbox" name="power[]" value="2" disabled
                @if (in_array('2',explode("|",$v->power))) checked @endif />&nbsp;栏目管理&nbsp;</label>
              <label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="3" disabled
                @if (in_array('3',explode("|",$v->power))) checked @endif />&nbsp;内容管理&nbsp;</label>
              <label class="checkbox-inline"><input type="checkbox" name="power[]" value="4" disabled
                @if (in_array('4',explode("|",$v->power))) checked @endif />&nbsp;广告管理&nbsp;</label>
              <label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="5" disabled
                @if (in_array('5',explode("|",$v->power))) checked @endif />&nbsp;数据库管理&nbsp;</label>
              <label class="checkbox-inline"><input type="checkbox" name="power[]" value="6" disabled
                @if (in_array('6',explode("|",$v->power))) checked @endif />&nbsp;资源管理&nbsp;</label>
              <label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="7" disabled
                @if (in_array('7',explode("|",$v->power))) checked @endif />&nbsp;友情链接管理&nbsp;</label>
              <label class="checkbox-inline"><input  type="checkbox"  name="power[]" value="8" disabled
                @if (in_array('8',explode("|",$v->power))) checked @endif />&nbsp;用户管理&nbsp;</label>
            </td>
          <td>
            <a class="btn btn-warning" href="{{url('admin/roles/edit',['id'=>$v->id])}}" style="padding:1px 6px;">修改</a>
            <a href="javascript:if(confirm('确认删除吗？')){ location.href='{{url('admin/roles/del',['id'=>$v->id])}}';}"><button type="button" class="btn btn-danger" style="padding:1px 6px;">删除</button></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="listPage">
    {{$cols->appends($_GET)->links()}}
  </div>
@endsection