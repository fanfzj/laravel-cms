@extends('layouts.admin')
@section('title',"添加角色")
@section('main')
	<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="#">首页</a></li>
      <li><a href="{{url('admin/roles/index')}}">角色</a></li>
      <li class="active">添加</li>
    </ol>
  	</div>
  	<div class="alert alert-success alert-dismissable hide">
            <button type="button" class="close" onclick="$('.alert').addClass('hide')">
                &times;
            </button>
            <b></b>
    </div>
	<form class="form-horizontal" id="form1" method="post" action="{{ url('admin/roles/doadd') }}">
				<div class="form-group">
					<label for="username" class="col-sm-1 control-label">角色名：</label>
					<div class="col-sm-6">
						<input type="text" name="name" class="form-control" id="name" placeholder="请填写角色名">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">权限：</label>
					<div class="col-sm-11">
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="0"/>&nbsp;系统设置&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="1"/>&nbsp;模型管理&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="2"/>&nbsp;栏目管理&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="3"/>&nbsp;内容管理&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="4"/>&nbsp;广告管理&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="5"/>&nbsp;数据库管理&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="6"/>&nbsp;资源管理&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="7"/>&nbsp;友情链接管理&nbsp;</label>
						<label class="checkbox-inline"><input type="checkbox" name="power[]" value="8"/>&nbsp;用户管理&nbsp;</label>
					</div>
				</div>
			<div class="form-group">
				<div class="col-sm-offset-1 col-sm-11">
					<button type="submit" style="margin:30px 15px 0 0;" class="btn btn-danger dropdown-toggle" id="addRowbtn">
					<span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>保存
				</button>
				</div>
	  </div>
		{{csrf_field()}}
	</form>
@endsection                