@extends('layouts/admin')
@section('title',"后台首页")
@section('main')
<div class="market-updates">
      <div class="col-md-3 col-sm-6 market-update-gd">
        <div class="market-update-block clr-block-1">
          <div class="col-md-8 market-update-left">
            <h3>{{ $user_num }}</h3>
            <h4>用户个数</h4>
            <p>管理员创建的用户总个数</p>
          </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-user-o"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 market-update-gd">
        <div class="market-update-block clr-block-2">
         <div class="col-md-8 market-update-left">
          <h3>{{ $category_num }}</h3>
          <h4>栏目个数</h4>
          <p>管理员创建的栏目总个数</p>
          </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-file-text-o"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 market-update-gd">
        <div class="market-update-block clr-block-3">
          <div class="col-md-8 market-update-left">
            <h3>{{ $adv_num }}</h3>
          <h4>广告个数</h4>
          <p>管理员创建的栏目总个数</p>
          </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-eye"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 market-update-gd">
        <div class="market-update-block clr-block-4">
          <div class="col-md-8 market-update-left">
            <h3>{{ $friend_num }}</h3>
          <h4>友链个数</h4>
          <p>管理员创建的友情链接总个数</p>
          </div>
          <div class="col-md-4 market-update-right">
            <i class="fa fa-users"> </i>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
       <div class="clearfix"> </div>
    </div>
<!--market updates end here-->
<!--mainpage chit-chating-->
<div class="chit-chat-layer1">
  <div class="col-md-12 chit-chat-layer1-left">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  栏目及内容数量统计
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>栏目名称</th>
                                      <th>类型</th>                 
                                      <th>页面类型</th>
                                      <th>导航显示</th>
                                      <th>内容数量</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($category_arr as $v)
                                    <tr>
                                      <td>{{$v['cnamestr']}}</td>
                                      <td>{{$v['pagestyle']}}</td>
                                      <td>{{$v['modelname']}}</td>
                                      <td>{{$v['isshow']}}</td>
                                      <td>{{ $v['content_count'] }}</td>
                                    </tr>
                                    @endforeach
                          </tbody>
                      </table>
                  </div>
             </div>
      </div>
     <div class="clearfix"> </div>
</div>
@endsection