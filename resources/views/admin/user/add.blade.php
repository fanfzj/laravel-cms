@extends('layouts.admin')
@section('title',"添加用户")
@section('main')
	<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="#">首页</a></li>
      <li><a href="{{url('admin/user/index')}}">用户</a></li>
      <li class="active">添加</li>
    </ol>
  	</div>
  	<div class="alert alert-success alert-dismissable hide">
            <button type="button" class="close" onclick="$('.alert').addClass('hide')">
                &times;
            </button>
            <b></b>
    </div>
	<form class="form-horizontal" id="form1" method="post" action="{{ url('admin/user/doadd') }}">
				<div class="form-group">
					<label for="username" class="col-sm-1 control-label">用户名：</label>
					<div class="col-sm-6">
						<input type="text" name="username" class="form-control" id="username" placeholder="用户名">
					</div>
					<span class="col-sm-5 help-block">请填写用户名</span>
				</div>
				<div class="form-group">
					<label for="inputPassword" class="col-sm-1 control-label">密码：</label>
					<div class="col-sm-6">
						<input type="password" class="form-control" id="inputPassword" name="password" placeholder="密码">
					</div>
					<span class="col-sm-5 help-block">请填写密码</span>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">角色：</label>
					<div class="col-sm-11">
						@foreach ($roles as $item)
						<label class="checkbox-inline"><input type="checkbox" name="roles[]" value="{{ $item->id }}"/>&nbsp;{{ $item->name }}&nbsp;</label>
						@endforeach
					</div>
				</div>
			<div class="form-group">
				<div class="col-sm-offset-1 col-sm-11">
					<button type="submit" style="margin:30px 15px 0 0;" class="btn btn-danger dropdown-toggle" id="addRowbtn">
					<span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>保存
				</button>
				</div>
	  </div>
		{{csrf_field()}}
	</form>
@endsection                