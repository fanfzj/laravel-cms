@extends('layouts.admin')
@section('title','用户管理')
@section('main')
  <div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li><a href="{{url('admin/user/index')}}">用户管理</a></li>
    </ol>
  </div>
  <div class="oper">
    <a href="{{url('admin/user/add')}}"><button type="button" class="btn btn-danger dropdown-toggle" id="addRowbtn"><span class="glyphicon  glyphicon-plus" aria-hidden="true"></span>添加用户</button></a>
  </div>
  <div class="tableList">
    <table class="table table-bordered table-striped table-hover">
      <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-1">
        <col class="col-xs-8">
        <col class="col-xs-2">
      </colgroup>
      <thead>
        <tr class="active">
          <th>用户名</th>
          <th>上次登录时间</th>
          <th>角色</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        @foreach($cols as $v)
        <tr>
          <td>{{$v->username}}</td>
          <td>@php echo date("Y-m-d H:i",$v->lastlogintime);  @endphp</td>
          <td>
            @php
             
            @endphp 
            	@foreach ($roles as $item)
              <label class="checkbox-inline"><input type="checkbox" name="roles[]" value="{{ $item->id }}" disabled  @if(in_array($item->id,Data::getRolesList($v->roles))) checked @endif  />&nbsp;{{ $item->name }}&nbsp;</label>
              @endforeach
        </td>
          <td>
            <a class="btn btn-warning" href="{{url('admin/user/edit',['id'=>$v->id])}}" style="padding:1px 6px;">修改</a>
            <a href="javascript:if(confirm('确认删除吗？')){ location.href='{{url('admin/user/del',['id'=>$v->id])}}';}"><button type="button" class="btn btn-danger" style="padding:1px 6px;">删除</button></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="listPage">
    {{$cols->appends($_GET)->links()}}
  </div>
@endsection