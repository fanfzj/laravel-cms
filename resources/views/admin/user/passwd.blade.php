@extends('layouts/admin')
@section('title',"修改密码")
@section('main')
<div class="nav">
    <ol class="breadcrumb" style="background: #f1f1f1;">
      <li><span style="color:orange;margin-right: 10px;" class="glyphicon glyphicon-home"></span><a href="{{url('admin')}}">首页</a></li>
      <li>修改密码</li>
    </ol>
  </div>
<form class="form-horizontal" action="{{url('admin/user/dopassword')}}" method="post">
   <div class="tab-pane fade in active" id="info">
      <input type="hidden" value="{{ $ob->id }}" name="id">
    <div class="form-group">
      <label class="col-sm-2 control-label">原始密码：</label>
      <div class="col-sm-5">
        <input type="password" value="" class="form-control" name='oldpassword' placeholder="请输入原始密码">
      </div>
      <span class="col-sm-5 help-block">请输入原始密码</span>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">新密码：</label>
      <div class="col-sm-5">
        <input type="password" class="form-control" name="password"  placeholder="请输入新密码" value="index">
      </div>
      <span class="col-sm-5 help-block">请输入新密码</span>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-info">保存</button>
        {{csrf_field()}}
      </div>
  </div>
   </div>
</form>
@endsection