<!DOCTYPE HTML>
<html>
<head>
<title>登录-{{ $site->sitename }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="{{ $site->keyword }}" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="{{asset('js/jquery-2.1.1.min.js')}}"></script> 
<!--icons-css-->
<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet"> 
<!--Google Fonts-->
<link href='https://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--static chart-->
</head>
<body>	
<div class="login-page">
    <div class="login-main">  	
    	 <div class="login-head">
				<h1>管理员登录</h1>
			</div>
			<div class="login-block">
				<form action="{{url('admin/check')}}" method="post">
					<input type="text" name="username" placeholder="用户名" required="请填写用户名">
					<input type="password" name="password" class="lock" placeholder="密码">
					<span class="help-block">{{session()->get('message','')}}</span>
					<input type="submit" value="登录">	
					{{csrf_field()}}
				</form>
			</div>
      </div>
</div>
<!--inner block end here-->

<!--scrolling js-->
		<script src="{{asset('js/jquery.nicescroll.js')}}"></script>
		<script src="{{asset('js/scripts.js')}}"></script>
		<!--//scrolling js-->
<script src="{{asset('js/bootstrap.js')}}"> </script>
<!-- mother grid end here-->
</body>
</html>


                      
						
